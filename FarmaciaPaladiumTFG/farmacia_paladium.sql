CREATE DATABASE IF NOT EXISTS farmaciapaladium;
USE farmaciapaladium;

CREATE TABLE IF NOT EXISTS registro 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre_usuario VARCHAR(50),
	password_registro VARCHAR(50),
	tipo_usuario VARCHAR(50)
	
);



CREATE TABLE IF NOT EXISTS producto 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    codigo_producto INT,
	nombre VARCHAR(50) NOT NULL,
	laboratorio VARCHAR(50),
    estado VARCHAR(20) NOT NULL,
    tipo VARCHAR(35) NOT NULL,
	stock VARCHAR(20) NOT NULL,
    precio FLOAT DEFAULT 0,
    fecha_fabricacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP()
);

CREATE TABLE IF NOT EXISTS cliente 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre_cliente VARCHAR(50) NOT NULL,
    apellidos_cliente VARCHAR(70) NOT NULL,
	telefono VARCHAR(9),
    email VARCHAR(30),
    dni VARCHAR(10) NOT NULL,
    direccion VARCHAR(63) NOT NULL,
    estado VARCHAR(35) NOT NULL,
	numero_cliente INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS venta 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	estado_venta VARCHAR(50) NOT NULL,
    numero_cliente_venta INT DEFAULT 0,
	codigo_producto_venta INT,
    iva FLOAT DEFAULT 0,
    tipo_pago VARCHAR(20) NOT NULL,
    fecha_venta TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    tipo_moneda VARCHAR(35) NOT NULL,
    id_trabajador INT UNSIGNED REFERENCES trabajador,
    id_cliente INT UNSIGNED REFERENCES cliente
);

CREATE TABLE IF NOT EXISTS trabajador 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre_trabajador VARCHAR(50) NOT NULL,
    apellidos_trabajador VARCHAR(70) NOT NULL,
    dni_trabajador VARCHAR(10) NOT NULL,
    sueldo_trabajador FLOAT DEFAULT 0,
    dias_trabajados VARCHAR(23) NOT NULL,
    telefono_trabajador VARCHAR(9),
    direccion_trabajador VARCHAR(63) NOT NULL,
    cargo_trabajador VARCHAR(35) NOT NULL
);

CREATE TABLE IF NOT EXISTS producto_clientes
(
	id_cliente INT UNSIGNED REFERENCES clientes,
	id_producto INT UNSIGNED REFERENCES producto,
	PRIMARY KEY (id_cliente, id_producto)
);
CREATE TABLE IF NOT EXISTS producto_trabajador
(
	id_producto INT UNSIGNED REFERENCES producto,
	id_trabajador INT UNSIGNED REFERENCES trabajador,
	PRIMARY KEY (id_producto, id_trabajador)
);

