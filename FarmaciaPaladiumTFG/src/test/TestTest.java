package test;

import com.marioea.farmaciapaladium.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;


import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestTest {
    private static SessionFactory sessionFactory;
    private Session session;
    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Registro.class);
        configuracion.addAnnotatedClass(Trabajador.class);
        configuracion.addAnnotatedClass(Venta.class);



        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);



    }
    @BeforeEach
    void setUp() {
        conectar();
        sessionFactory=getSessionFactory();
        session=sessionFactory.openSession();

    }

    @AfterEach
    public void tearDown() {
        if (sessionFactory != null) sessionFactory.close();

    }

    /**
     * Test para comprobar el alta de un cliente
     */
    @Test
    public void altaCliente() {
        System.out.println("Cargando altaCliente...");

        session.beginTransaction();

        Cliente cliente = new Cliente("Ricardo","Morientes","976143567","fmorientes@gmail.com","12134556T","Calle Carmen","Activo",12);
        Integer id = (Integer) session.save(cliente);

        session.getTransaction().commit();

        Assertions.assertFalse(id > 0);
    }

    /**
     * Test para actualizar un cliente
     */
    @Test
    public void clienteActualizado() {
        System.out.println("Cargando cliente Actualizado...");

        Integer id = 7;
        Cliente cliente = new Cliente(id,"Ricardo","Pereira","976143567","fmorientes@gmail.com","12134556T","Calle Carmen","Activo",12);

        session.beginTransaction();
        session.update(cliente);
        session.getTransaction().commit();

        Cliente clienteActualizado = session.find(Cliente.class, id);

        assertEquals("Ricardo", clienteActualizado.getNombreCliente());
    }

    /**
     * Test para buscar un cliente
     */
    @Test
    public void testGetCliente() {
        System.out.println("Cargando testGetCliente...");

        Integer id = 2;

        Cliente cliente = session.find(Cliente.class, id);

        assertEquals("nn", cliente.getNombreCliente());
    }

    /**
     * Test para listar los clientes
     */
    @Test
    public void testListadoClientes() {
        System.out.println("Cargando testListadoClientes...");

        Query<Cliente> query = session.createQuery("from Cliente", Cliente.class);
        List<Cliente> listaResultados = query.getResultList();

        Assertions.assertFalse(listaResultados.isEmpty());
    }

    /**
     * Test para eliminar un cliente
     */
    @Test
    public void clienteEliminado() {
        System.out.println("Cargando eliminacion de Cliente...");

        Integer id = 6;
        Cliente cliente = session.find(Cliente.class, id);

        session.beginTransaction();
        session.delete(cliente);
        session.getTransaction().commit();

        Cliente clienteEliminado = session.find(Cliente.class, id);

        Assertions.assertNull(clienteEliminado);
    }

    /**
     * Test para comprobar el alta de un producto
     */
    //Producto
    @Test
    public void altaProducto() {
        System.out.println("Cargando altaProducto...");

        session.beginTransaction();
        Date dateProducto = new Date(105,6,8);
        Producto producto = new Producto(56,"Jacinto","Almazan del Obispo","jacinto@gmail.com","Cremas","45",23,dateProducto);
        Integer id = (Integer) session.save(producto);

        session.getTransaction().commit();

        Assertions.assertFalse(id > 0);
    }

    /**
     * Test para actualizar un producto
     */
    @Test
    public void productoActualizado() {
        System.out.println("Cargando producto Actualizado...");

        Integer id = 9;
        Date dateProducto = new Date(105,6,8);
        Producto producto = new Producto(id,56,"Jacinto","Physer","jacinto@gmail.com","Cremas","45",23,dateProducto);

        session.beginTransaction();
        session.update(producto);
        session.getTransaction().commit();

        Producto productoActualizado = session.find(Producto.class, id);

        assertEquals("Jacinto", productoActualizado.getNombre());
    }

    /**
     * Test para buscar un producto
     */
    @Test
    public void testGetProducto() {
        System.out.println("Cargando testGetProducto...");

        Integer id = 9;

        Producto producto = session.find(Producto.class, id);

        assertEquals("Jacinto", producto.getNombre());
    }

    /**
     * Test para listar los productos
     */
    @Test
    public void testListadoProductos() {
        System.out.println("Cargando testListadoProducto...");

        Query<Producto> query = session.createQuery("from Producto", Producto.class);
        List<Producto> listaResultados = query.getResultList();

        Assertions.assertFalse(listaResultados.isEmpty());
    }

    /**
     * Test para eliminar un producto
     */
    @Test
    public void productoEliminado() {
        System.out.println("Cargando eliminacion de Producto...");

        Integer id = 4;
        Producto producto = session.find(Producto.class, id);

        session.beginTransaction();
        session.delete(producto);
        session.getTransaction().commit();

        Producto productoEliminado = session.find(Producto.class, id);

        Assertions.assertNull(productoEliminado);
    }

    /**
     * Test para comprobar el alta de un trabajador
     */
    //Trabajador
    @Test
    public void altaTrabajador() {
        System.out.println("Cargando altaTrabajador...");
        session.beginTransaction();

        Trabajador trabajador = new Trabajador("Enrique","Pastor","12345612T",1200,"30","626708665","Paseo Calanda","Auxiliar Diplomado");
        Integer id = (Integer) session.save(trabajador);

        session.getTransaction().commit();

        Assertions.assertFalse(id > 0);
    }

    /**
     * Test para actualizar un trabajador
     */
      @Test
    public void trabajadorActualizado() {
        System.out.println("Cargando trabajador Actualizado...");

        Integer id = 5;

        Trabajador trabajador = new Trabajador(id,"Federico","Pastor","12345612T",1200,"30","626708665","Paseo Calanda","Auxiliar Diplomado");

        session.beginTransaction();
        session.update(trabajador);
        session.getTransaction().commit();

        Trabajador trabajadorActualizado = session.find(Trabajador.class, id);

        assertEquals("Federico", trabajadorActualizado.getNombreTrabajador());
    }

    /**
     * Test para buscar un trabajador
     */
    @Test
    public void testGetTrabajador() {
        System.out.println("Cargando testGetTrabajador...");

        Integer id = 5;

        Trabajador trabajador = session.find(Trabajador.class, id);

        assertEquals("Federico", trabajador.getNombreTrabajador());
    }

    /**
     * Test para listar los trabajadores
     */
    @Test
    public void testListadoTrabajador() {
        System.out.println("Cargando testListadoTrabajador...");

        Query<Trabajador> query = session.createQuery("from Trabajador", Trabajador.class);
        List<Trabajador> listaResultados = query.getResultList();

        Assertions.assertFalse(listaResultados.isEmpty());
    }

    /**
     * Test para eliminar un trabajador
     */
    @Test
    public void trabajadorEliminado() {
        System.out.println("Cargando eliminacion de Trabajador...");

        Integer id = 6;
        Trabajador trabajador = session.find(Trabajador.class, id);

        session.beginTransaction();
        session.delete(trabajador);
        session.getTransaction().commit();

        Trabajador trabajadorEliminado = session.find(Trabajador.class, id);

        Assertions.assertNull(trabajadorEliminado);
    }

    /**
     * Test para comprobar el alta de una venta
     */
    //Venta
    @Test
    public void altaVenta() {
        System.out.println("Cargando altaVenta...");
        Date dateVenta = new Date(105,6,8);
        Cliente cliente = new Cliente(6,"Ricardo","Morientes","976143567","fmorientes@gmail.com","12134556T","Calle Carmen","Activo",12);
        Trabajador trabajador = new Trabajador(5,"Enrique","Pastor","12345612T",1200,"30","626708665","Paseo Calanda","Auxiliar Diplomado");
        session.beginTransaction();
        Venta venta = new Venta("Abierta",123,678,33,"Efectivo",dateVenta,"Euro",cliente,trabajador);
        Integer id = (Integer) session.save(venta);

        session.getTransaction().commit();

        Assertions.assertFalse(id > 0);
    }

    /**
     * Test para actualizar una venta
     */
    @Test
    public void ventaActualizado() {
        System.out.println("Cargando venta Actualizado...");

        Integer id = 9;
        Date dateVenta = new Date(105,6,8);
        Cliente cliente = new Cliente(6,"Ricardo","Morientes","976143567","fmorientes@gmail.com","12134556T","Calle Carmen","Activo",12);
        Trabajador trabajador = new Trabajador(5,"Enrique","Pastor","12345612T",1200,"30","626708665","Paseo Calanda","Auxiliar Diplomado");
        Venta venta = new Venta(id,"Abierta",123,678,33,"Paypal",dateVenta,"Euro",cliente,trabajador);

        session.beginTransaction();
        session.update(venta);
        session.getTransaction().commit();

        Venta ventaActualizado = session.find(Venta.class, id);

        assertEquals("Abierta", ventaActualizado.getEstadoVenta());
    }

    /**
     * Test para buscar una venta
     */
    @Test
    public void testGetVenta() {
        System.out.println("Cargando testGetVenta...");

        Integer id = 9;

        Venta venta = session.find(Venta.class, id);

        assertEquals("Abierta", venta.getEstadoVenta());
    }

    /**
     * Test para listar una venta
     */
    @Test
    public void testListadoVenta() {
        System.out.println("Cargando testListadoVenta...");

        Query<Venta> query = session.createQuery("from Venta", Venta.class);
        List<Venta> listaResultados = query.getResultList();

        Assertions.assertFalse(listaResultados.isEmpty());
    }

    /**
     * Test para eliminar una venta
     */
    @Test
    public void trabajadorVenta() {
        System.out.println("Cargando eliminacion de Venta...");

        Integer id = 7;
        Venta venta = session.find(Venta.class, id);

        session.beginTransaction();
        session.delete(venta);
        session.getTransaction().commit();

        Venta ventaEliminado = session.find(Venta.class, id);

        Assertions.assertNull(ventaEliminado);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}