package com.marioea.farmaciapaladium.registro;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de TIpos de usuario de la vista.
 * Representan los diferentes roles de usuario de la aplicacion.
 */
public enum EnumeracionTipos {
    ADMINISTRADOR("Administrador"),
    CONSULTOR("Consultor"),
    VENDEDOR("Vendedor");
    private String permiso;

    EnumeracionTipos(String permiso) {
        this.permiso = permiso;
    }

    public String getPermiso() {
        return permiso;
    }
}
