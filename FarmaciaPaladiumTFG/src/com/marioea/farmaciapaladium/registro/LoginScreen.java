package com.marioea.farmaciapaladium.registro;

import javax.swing.*;
import java.awt.*;
/**
 * Esta es la ventana principal de la aplicacion donde debemos registrarnos
 * con nuestras cdrenciales de usuario y de ahi accedemos a las distintas pantallas de la aplicacion
 */
public class LoginScreen extends JFrame{

    private JPanel panel1;
    public JTextField txtPassword;
    public JTextField txtUser;
    public JComboBox comboTipoUsuario;
    public JButton loginbtn;
    public JButton resetbtn;
    public JProgressBar progressBar;

    /**
     * Constructor de la clase login screen
     * Donde se marca datos de la ventana como el tamaño, nombre de la ventana
     * y las distintas enumeraciones de la clase de registro
     */
    public LoginScreen(){
        this.setTitle("Farmacia Paladium");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("logo.png"));
        this.setIconImage(ico);

        enumeracion();


    }
    /**
     * Metodo que coge los datos de la clase enumerada de tipos de usuario
     * y los almacena en una combobox
     */
    public void enumeracion(){
        for (EnumeracionTipos enumeracionTipos: EnumeracionTipos.values()){
            comboTipoUsuario.addItem(enumeracionTipos.getPermiso());
        }
        comboTipoUsuario.setSelectedItem(-1);
    }
}
