package com.marioea.farmaciapaladium;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int id;
    private String nombreCliente;
    private String apellidosCliente;
    private String telefono;
    private String email;
    private String dni;
    private String direccion;
    private String estado;
    private int numeroCliente;
    private List<Venta> ventas;
    private List<Producto> productos;

    public Cliente() {

    }

    public Cliente(String nombreCliente, String apellidosCliente, String telefono, String email, String dni, String direccion, String estado, int numeroCliente) {
        this.nombreCliente = nombreCliente;
        this.apellidosCliente = apellidosCliente;
        this.telefono = telefono;
        this.email = email;
        this.dni = dni;
        this.direccion = direccion;
        this.estado = estado;
        this.numeroCliente = numeroCliente;
        
    }

    public Cliente(int id, String nombreCliente, String apellidosCliente, String telefono, String email, String dni, String direccion, String estado, int numeroCliente) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.apellidosCliente = apellidosCliente;
        this.telefono = telefono;
        this.email = email;
        this.dni = dni;
        this.direccion = direccion;
        this.estado = estado;
        this.numeroCliente = numeroCliente;
       
    }




    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_cliente")
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    @Basic
    @Column(name = "apellidos_cliente")
    public String getApellidosCliente() {
        return apellidosCliente;
    }

    public void setApellidosCliente(String apellidosCliente) {
        this.apellidosCliente = apellidosCliente;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "estado")
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "numero_cliente")
    public int getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(int numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                numeroCliente == cliente.numeroCliente &&
                Objects.equals(nombreCliente, cliente.nombreCliente) &&
                Objects.equals(apellidosCliente, cliente.apellidosCliente) &&
                Objects.equals(telefono, cliente.telefono) &&
                Objects.equals(email, cliente.email) &&
                Objects.equals(dni, cliente.dni) &&
                Objects.equals(direccion, cliente.direccion) &&
                Objects.equals(estado, cliente.estado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreCliente, apellidosCliente, telefono, email, dni, direccion, estado, numeroCliente);
    }

    @OneToMany(mappedBy = "cliente")
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "producto_clientes", catalog = "", schema = "farmaciapaladium", joinColumns = @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return
                "Nombre: '" + nombreCliente + '\'' +
                ", Apellidos: '" + apellidosCliente + '\'' +
                ", DNI: '" + dni + '\'';
    }
}
