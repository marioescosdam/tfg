package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de Estado de Producto de la vista.
 * Representan los diferentes estados de producto de la aplicacion.
 */
public enum EnumeracionEstadoProducto {
    INTRODUCCION("Introduccion"),
    CRECIMIENTO("Crecimiento"),
    MADUREZ("Madurez"),
    DECLIEVE("Declieve");
    private String estadoProducto;

    EnumeracionEstadoProducto(String estadoProducto) {
        this.estadoProducto = estadoProducto;
    }

    public String getEstadoProducto() {
        return estadoProducto;
    }
}
