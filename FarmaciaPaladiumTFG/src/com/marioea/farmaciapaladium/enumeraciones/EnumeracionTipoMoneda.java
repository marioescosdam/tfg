package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de TIpo de moneda de la vista.
 * Representan los diferentes tipos de moneda en la pantalla de venta de la aplicacion.
 */
public enum EnumeracionTipoMoneda {
    DOLAR("Dolar"),
    EURO("Euro"),
    YENJAPONES("Yen"),
    LIBRAESTERLINA("Libra Esterlina"),
    FRANCOSUIZO("Franco Suizo");
    private String tipoMoneda;

    EnumeracionTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }
}
