package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de estado de venta de la vista.
 * Representan los diferentes estados en los que se encuentra la venta de la aplicacion.
 */
public enum EnumeracionEstadoVenta {
    ABIERTA("Abierta"),
    CERRADA("Cerrada"),
    CANCELADA("Cancelada");
    private String estadoVenta;

    EnumeracionEstadoVenta(String estadoVenta) {
        this.estadoVenta = estadoVenta;
    }

    public String getEstadoVenta() {
        return estadoVenta;
    }
}
