package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de TIpo de producto de la vista.
 * Representan los diferentes tipos de producto de la aplicacion.
 */
public enum EnumeracionTipoProducto {
    CREMAS("Cremas"),
    MAQUILLAJE("Maquillaje"),
    JABONES("Jabones"),
    DEPILATORIOS("Depilatorios"),
    DESODORANTES("Desodorantes"),
    MEDICAMENTOS("Medicamentos"),
    BLISTER("Blister"),
    DENTRIFICOS("Dentrificos"),
    PERFUMES("Perfumes");
    private String tipoProducto;

    EnumeracionTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String gettipoProducto() {
        return tipoProducto;
    }
}
