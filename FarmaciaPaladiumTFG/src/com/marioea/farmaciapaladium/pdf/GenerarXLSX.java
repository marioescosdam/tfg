package com.marioea.farmaciapaladium.pdf;

import com.marioea.farmaciapaladium.gui.Modelo;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.view.JasperViewer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GenerarXLSX {
    Connection connection;
    /**
     * Metodo que se encarga de generar un xlsx de la tabla de cliente a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXCliente(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\ClientesJRXML.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //generamos un exportador a xlsx
            JRXlsxExporter exporter = new JRXlsxExporter();
            //exportamos el jasper
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            //guardamos el xlsx en la carpeta de documentos generados
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\clientesPaladium.xlsx");
            exporter.exportReport();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un diagrama en xlsx de la tabla de cliente a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXClienteDiagrama(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoCliente.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\DiagramaclientesPaladium.xlsx");
            exporter.exportReport();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un xlsx de la tabla de producto a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXProducto(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\ProductosJRXML.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\productosPaladium.xlsx");
            exporter.exportReport();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un diagrama en xlsx de la tabla de producto a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXProductoDiagrama(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoProducto.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\DiagramaproductosPaladium.xlsx");
            exporter.exportReport();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un xlsx de la tabla de trabajador a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXTrabajador(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\TrabajadoresJRXML.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\trabajadorPaladium.xlsx");
            exporter.exportReport();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un diagrama en xlsx de la tabla de trabajador a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXTrabajadorDiagrama(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoTrabajador.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\DiagramatrabajadorPaladium.xlsx");
            exporter.exportReport();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un xlsx de la tabla de venta a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXVenta(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\VentasJRXML.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\ventasPaladium.xlsx");
            exporter.exportReport();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     *  Metodo que se encarga de generar un diagrama en xlsx de la tabla de venta a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarXLSXVentaDiagrama(Modelo modelo) throws SQLException {

        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoVentas.jrxml");
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "DocumentosGenerados\\DiagramaventasPaladium.xlsx");
            exporter.exportReport();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
