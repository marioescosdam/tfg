package com.marioea.farmaciapaladium.pdf;

import com.marioea.farmaciapaladium.gui.Modelo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Metodo que mediante un archivo de jrxml lo compila a jasper y lo visualiza con el visor
 * y genera un archivo pdf con los datos que se encuentran en ese jasper
 */
public class GenerarPDF {
    //Creamos una variable de conexion
    Connection connection;

    /**
     * Metodo que se encarga de generar el pdf de la tabla de cliente a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfCliente(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\ClientesJRXML.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\ClientesPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar un diagrama en pdf de la tabla de cliente a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfClienteDiagrama(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoCliente.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\DiagramaClientesPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar el pdf de la tabla de producto a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfProducto(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\ProductosJRXML.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\ProductosPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar un diagrama en pdf de la tabla de producto a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfProductoDiagrama(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoProducto.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\DiagramaProductosPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar el pdf de la tabla trabajador a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfTrabajador(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\TrabajadoresJRXML.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\TrabajadoresPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar un diagrama en pdf de la tabla trabajador a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfTrabajadorDiagrama(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoTrabajador.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\DiagramaTrabajadoresPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar el pdf de la tabla de venta a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfVenta(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\VentasJRXML.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\VentasPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que se encarga de generar un diagrama en pdf de la tabla de venta a traves de un archivo jasper
     * @param modelo
     * @throws SQLException
     */
    public void generarpdfVentaDiagrama(Modelo modelo) throws SQLException {
        //desde la variable de conexion coge la conexion de la base de datos
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //Compilamos el archivo JRXML para convertirlo en jasper
            JasperReport reporte= JasperCompileManager.compileReport("src\\informes\\QuesitoVentas.jrxml");
            //Pintamos el jasper y cogemos tambien la conexion de la base de datos
            JasperPrint print= JasperFillManager.fillReport(reporte,null,connection);
            //visualizamos el informe
            JasperViewer jasperViewer= new JasperViewer(print,false);
            jasperViewer.show();
            //exportamos el jasper a pdf y lo guardamos en la carpeta de documentos generados
            JasperExportManager.exportReportToPdfFile(print, "DocumentosGenerados\\DiagramaVentasPaladium.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

}
