package com.marioea.farmaciapaladium;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Trabajador {
    private int id;
    private String nombreTrabajador;
    private String apellidosTrabajador;
    private String dniTrabajador;
    private double sueldoTrabajador;
    private String diasTrabajados;
    private String telefonoTrabajador;
    private String direccionTrabajador;
    private String cargoTrabajador;
    private List<Venta> ventas;
    private List<Producto> productos;


    public Trabajador() {
    }

    public Trabajador(int id, String nombreTrabajador, String apellidosTrabajador, String dniTrabajador, double sueldoTrabajador, String diasTrabajados, String telefonoTrabajador, String direccionTrabajador, String cargoTrabajador) {
        this.id = id;
        this.nombreTrabajador = nombreTrabajador;
        this.apellidosTrabajador = apellidosTrabajador;
        this.dniTrabajador = dniTrabajador;
        this.sueldoTrabajador = sueldoTrabajador;
        this.diasTrabajados = diasTrabajados;
        this.telefonoTrabajador = telefonoTrabajador;
        this.direccionTrabajador = direccionTrabajador;
        this.cargoTrabajador = cargoTrabajador;
    }
    public Trabajador( String nombreTrabajador, String apellidosTrabajador, String dniTrabajador, double sueldoTrabajador, String diasTrabajados, String telefonoTrabajador, String direccionTrabajador, String cargoTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
        this.apellidosTrabajador = apellidosTrabajador;
        this.dniTrabajador = dniTrabajador;
        this.sueldoTrabajador = sueldoTrabajador;
        this.diasTrabajados = diasTrabajados;
        this.telefonoTrabajador = telefonoTrabajador;
        this.direccionTrabajador = direccionTrabajador;
        this.cargoTrabajador = cargoTrabajador;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_trabajador")
    public String getNombreTrabajador() {
        return nombreTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
    }

    @Basic
    @Column(name = "apellidos_trabajador")
    public String getApellidosTrabajador() {
        return apellidosTrabajador;
    }

    public void setApellidosTrabajador(String apellidosTrabajador) {
        this.apellidosTrabajador = apellidosTrabajador;
    }

    @Basic
    @Column(name = "dni_trabajador")
    public String getDniTrabajador() {
        return dniTrabajador;
    }

    public void setDniTrabajador(String dniTrabajador) {
        this.dniTrabajador = dniTrabajador;
    }

    @Basic
    @Column(name = "sueldo_trabajador")
    public double getSueldoTrabajador() {
        return sueldoTrabajador;
    }

    public void setSueldoTrabajador(double sueldoTrabajador) {
        this.sueldoTrabajador = sueldoTrabajador;
    }

    @Basic
    @Column(name = "dias_trabajados")
    public String getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(String diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    @Basic
    @Column(name = "telefono_trabajador")
    public String getTelefonoTrabajador() {
        return telefonoTrabajador;
    }

    public void setTelefonoTrabajador(String telefonoTrabajador) {
        this.telefonoTrabajador = telefonoTrabajador;
    }

    @Basic
    @Column(name = "direccion_trabajador")
    public String getDireccionTrabajador() {
        return direccionTrabajador;
    }

    public void setDireccionTrabajador(String direccionTrabajador) {
        this.direccionTrabajador = direccionTrabajador;
    }

    @Basic
    @Column(name = "cargo_trabajador")
    public String getCargoTrabajador() {
        return cargoTrabajador;
    }

    public void setCargoTrabajador(String cargoTrabajador) {
        this.cargoTrabajador = cargoTrabajador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trabajador that = (Trabajador) o;
        return id == that.id &&
                Double.compare(that.sueldoTrabajador, sueldoTrabajador) == 0 &&
                Objects.equals(nombreTrabajador, that.nombreTrabajador) &&
                Objects.equals(apellidosTrabajador, that.apellidosTrabajador) &&
                Objects.equals(dniTrabajador, that.dniTrabajador) &&
                Objects.equals(diasTrabajados, that.diasTrabajados) &&
                Objects.equals(telefonoTrabajador, that.telefonoTrabajador) &&
                Objects.equals(direccionTrabajador, that.direccionTrabajador) &&
                Objects.equals(cargoTrabajador, that.cargoTrabajador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreTrabajador, apellidosTrabajador, dniTrabajador, sueldoTrabajador, diasTrabajados, telefonoTrabajador, direccionTrabajador, cargoTrabajador);
    }

    @OneToMany(mappedBy = "trabajador")
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @Fetch(value= FetchMode.SUBSELECT)
    @JoinTable(name = "producto_trabajador", catalog = "", schema = "farmaciapaladium", joinColumns = @JoinColumn(name = "id_trabajador", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    @Override
    public String toString() {
        return "Nombre:'" + nombreTrabajador + '\'' +
                ", Apellidos: '" + apellidosTrabajador + '\'' +
                ", DNI: '" + dniTrabajador + '\'' +
                ", Dias Trabajados: '"+diasTrabajados+'\'' +
                ", Cargo del Trabajador: '" + cargoTrabajador + '\'';
    }
}
