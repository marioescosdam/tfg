package com.marioea.farmaciapaladium.admin;

import com.github.lgooddatepicker.components.DatePicker;

import com.marioea.farmaciapaladium.enumeraciones.*;
import com.marioea.farmaciapaladium.gui.Modelo;
import com.marioea.farmaciapaladium.listar.Listado;
import com.marioea.farmaciapaladium.registro.EnumeracionTipos;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.shaper.StandardButtonShaper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
/**
 * Esta ventana corresponde al usuario con un permiso de administrador
 * En ella se almacenaran todas las clases de la aplicacion y tambien se mandaran indicencias mediante el correo electronico
 */
public class AdminScreen extends JFrame {


    private JTabbedPane tabbedPane1;
     private JPanel panel1;
    public JTextField txtCodigoProducto;
    public JTextField txtNombreProducto;
    public JTextField txtLaboratorio;
    public JComboBox comboEstadoProducto;
    public JComboBox comboTipoProducto;
    public JTextField txtStock;
    public JTextField txtPrecioProducto;
    public DatePicker datePickerProducto;
    public JButton altaProductobtn;
    public JButton modificarProductobtn;
    public  JButton eliminarProductobtn;
    public JList listProducto;
    public JButton regreso;
    public JTextField txtNombreCliente;
    public JTextField txtApellidoCliente;
    public JTextField txtTelefonoCliente;
    public JTextField txtEmailCliente;
    public JTextField txtDniCliente;
    public JTextField txtDireccionCliente;
    public JComboBox comboEstadoCliente;
    public JTextField txtNumeroCliente;
    public JButton altaClientebtn;
    public JButton modificarClientebtn;
    public JButton eliminarClientebtn;
    public JList listCliente;
    public JTextField txtNombreTrabajador;
    public JTextField txtApellidosTrabajador;
    public JTextField txtDniTrabajador;
    public JTextField txtSueldoTrabajador;
    public JTextField txtDiasTrabajados;
    public JTextField txtTelefonoTrabajador;
    public JTextField txtDireccionTrabajador;
    public JComboBox comboCargoTrabajador;
    public JButton altaTrabajadorbtn;
    public JButton modificarTrabajadorbtn;
    public JButton eliminarTrabajadorbtn;
    public JList listTrabajador;
    public JComboBox comboEstadoVenta;
    public JTextField txtNumeroClienteVenta;
    public JTextField txtCodigoProductoVenta;
    public JTextField txtIvaVenta;
    public JComboBox comboTipoPagoVenta;
    public DatePicker dateFechaVenta;
    public JComboBox comboTipoMonedaVenta;
    public JComboBox comboTrabajador;
    public JButton generarVentabtn;
    public JButton modificarVentabtn;
    public JButton eliminarVentabtn;
    public JList listVenta;
    public JButton listarClienteProductobtn;
    public JList listClienteProducto;
    public JButton asignarProductoClientebtn;
    public JList listProductoCliente;
    public JComboBox comboCliente;
    public JButton generarPDFClientebtn;
    public JButton generarPDFProductobtn;
    public JButton generarPDFTrabajadorbtn;
    public JButton generarPDFVentabtn;
    public JButton listarTrabajadorProductobtn;
    public JList listTrabajadorProducto;
    public JButton asignarProductoTrabajadorbtn;
    public JList listProductoTrabajador;
    public JButton generarExcelClientebtn;
    public JButton generarExcelProductobtn;
    public JButton generarExcelTrabajadorbtn;
    public JButton generarExcelVentabtn;
    public JTextField txtNuevoUsuarioAdmin;
    public JPasswordField txtNuevaPasswordAdmin;
    public JComboBox comboTiposUsuario;
    public JButton nuevoUsuarioAdminbtn;
    public JButton modificarUsuarioAdminbtn;
    public JButton eliminarUsuarioAdminbtn;
    public JList listNuevoUsuarioAdmin;
    public JButton generarCodigosBarrasbtn;
    public JLabel lblCodigoProducto;
    public JTextField txtDestinatarioAdmin;
    public JTextField txtAsuntoAdmin;
    public JTextArea textAreaMensajeAdmin;
    public JButton enviarMailAdminbtn;
    public JButton diagramaClientebtn;
    public JButton diagramaProductobtn;
    public JButton diagramaTrabajadorbtn;
    public JButton diagramaVentabtn;
    public JButton generarCodigosBarrasVentabtn;
    public JButton buscarClientebtn;
    public JTextField txtBuscarCliente;
    public JButton buscarProductoPorCodigobtn;
    public JTextField txtBuscarproductoAdmin;
    public JButton buscarTrabajadorPorDiasbtn;
    public JTextField txtBuscarTrabajadorAdmin;
    public JButton buscarVentaPorTipobtn;
    public JTextField txtBuscarVentaAdmin;
    public JButton listarClientesbtn;
    public JButton listarProductosbtn;
    public JButton listarTrabajadorbtn;
    public JButton listarVentasbtn;
    public JMenuItem acercaDeItem;
    public JMenuItem ayudaItem;



    public  DefaultListModel dlmCliente;
    public  DefaultListModel dlmProducto;
    public  DefaultListModel dlmTrabajador;
    public  DefaultListModel dlmVenta;
    public  DefaultListModel dlmRegistro;
    public  DefaultListModel dlmClienteProducto;
    public  DefaultListModel dlmProductoCliente;
    public  DefaultListModel dlmTrabajadorProducto;
    public  DefaultListModel dlmProductoTrabajador;
    /**
     * Constructor de la clase admin
     * Donde se marca datos de la ventana como el tamaño, nombre de la ventana
     * y se da funcionamiento al menu de la parte superior y las distintas enumeraciones de la clase admin
     */

    public AdminScreen(){
        this.setTitle("Administrador");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()+50));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("logo.png"));
        this.setIconImage(ico);

        crearMenu();
        crearModelos();
        enumeracionEstadoCliente();
        enumeracionEstadoProducto();
        enumeracionTipoProducto();
        enumeracionCargoTrabajador();
        enumeracionEstadoVenta();
        enumeracionTipoPago();
        enumeracionTipoMoneda();
        enumeracion();

        /**
         * Limitadores de tamaño de textfield
         */
        txtDniCliente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDniCliente.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDniTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDniTrabajador.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });

        txtDireccionCliente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDireccionCliente.getText().length()>=45){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtTelefonoCliente.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtTelefonoCliente.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtCodigoProducto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtCodigoProducto.getText().length()>=5){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtLaboratorio.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtLaboratorio.getText().length()>=20){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtPrecioProducto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtPrecioProducto.getText().length()>=4){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtStock.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtStock.getText().length()>=6){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDniTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDniTrabajador.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtTelefonoTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtTelefonoTrabajador.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDireccionTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDireccionTrabajador.getText().length()>=45){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtSueldoTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtSueldoTrabajador.getText().length()>=4){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDiasTrabajados.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDiasTrabajados.getText().length()>=3){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });

    }

    /**
     * Le damos a las listar creadas un default list model
     */
    private void crearModelos() {
        dlmCliente = new DefaultListModel();
        listCliente.setModel(dlmCliente);
        dlmProducto = new DefaultListModel();
        listProducto.setModel(dlmProducto);
        dlmTrabajador = new DefaultListModel();
        listTrabajador.setModel(dlmTrabajador);
        dlmVenta = new DefaultListModel();
        listVenta.setModel(dlmVenta);
        dlmRegistro = new DefaultListModel();
        listNuevoUsuarioAdmin.setModel(dlmRegistro);
        dlmClienteProducto = new DefaultListModel();
        listClienteProducto.setModel(dlmClienteProducto);
        dlmProductoCliente = new DefaultListModel();
        listProductoCliente.setModel(dlmProductoCliente);
        dlmTrabajadorProducto= new DefaultListModel();
        listTrabajadorProducto.setModel(dlmTrabajadorProducto);
        dlmProductoTrabajador= new DefaultListModel();
        listProductoTrabajador.setModel(dlmProductoTrabajador);

    }
    /**
     * Barra de menu que se genera en la parte superior de la ventana
     * Incluye datos como el acerca de y la ayuda
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");

        acercaDeItem = new JMenuItem("Acerca de");
        acercaDeItem.setActionCommand("Acerca de");
        ayudaItem = new JMenuItem("Ayuda");
        ayudaItem.setActionCommand("Ayuda");


        menu.add(acercaDeItem);
        menu.add(ayudaItem);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del estado de cliente del administrador
     * y los almacena en una combobox
     */
    public void enumeracionEstadoCliente(){
        for (EnumeracionEstadoCliente enumeracionEstadoCliente: EnumeracionEstadoCliente.values()){
            comboEstadoCliente.addItem(enumeracionEstadoCliente.getEstadoCliente());
        }
        comboEstadoCliente.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del estado del producto del administrador
     * y los almacena en una combobox
     */
    public void enumeracionEstadoProducto(){
        for (EnumeracionEstadoProducto enumeracionEstadoProducto: EnumeracionEstadoProducto.values()){
            comboEstadoProducto.addItem(enumeracionEstadoProducto.getEstadoProducto());
        }
        comboEstadoProducto.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del tipo de producto del administrador
     * y los almacena en una combobox
     */
    public void enumeracionTipoProducto(){
        for (EnumeracionTipoProducto enumeracionTipoProducto: EnumeracionTipoProducto.values()){
            comboTipoProducto.addItem(enumeracionTipoProducto.gettipoProducto());
        }
        comboTipoProducto.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del cargo de trabajador del administrador
     * y los almacena en una combobox
     */
    public void enumeracionCargoTrabajador(){
        for (EnumeracionCargoTrabajador enumeracionCargoTrabajador: EnumeracionCargoTrabajador.values()){
            comboCargoTrabajador.addItem(enumeracionCargoTrabajador.getCargoTrabajador());
        }
        comboCargoTrabajador.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del estado de venta del administrador
     * y los almacena en una combobox
     */
    public void enumeracionEstadoVenta(){
        for (EnumeracionEstadoVenta enumeracionEstadoVenta: EnumeracionEstadoVenta.values()){
            comboEstadoVenta.addItem(enumeracionEstadoVenta.getEstadoVenta());
        }
        comboEstadoVenta.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del tipo de pago del administrador
     * y los almacena en una combobox
     */
    public void enumeracionTipoPago(){
        for (EnumeracionTipoPago enumeracionTipoPago: EnumeracionTipoPago.values()){
            comboTipoPagoVenta.addItem(enumeracionTipoPago.getTipoPago());
        }
        comboTipoPagoVenta.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del tipo de moneda del administrador
     * y los almacena en una combobox
     */
    public void enumeracionTipoMoneda(){
        for (EnumeracionTipoMoneda enumeracionTipoMoneda: EnumeracionTipoMoneda.values()){
            comboTipoMonedaVenta.addItem(enumeracionTipoMoneda.getTipoMoneda());
        }
        comboTipoMonedaVenta.setSelectedItem(-1);
    }
    /**
     * Metodo que coge los datos de la clase enumerada del tipos de usuario del administrador
     * y los almacena en una combobox
     */
    public void enumeracion(){
        for (EnumeracionTipos enumeracionTipos: EnumeracionTipos.values()){
            comboTiposUsuario.addItem(enumeracionTipos.getPermiso());
        }
        comboTiposUsuario.setSelectedItem(-1);
    }



}
