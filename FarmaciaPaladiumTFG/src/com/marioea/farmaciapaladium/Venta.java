package com.marioea.farmaciapaladium;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Venta {
    private int id;
    private String estadoVenta;
    private int numeroClienteVenta;
    private int codigoProductoVenta;
    private double iva;
    private String tipoPago;
    private Date fechaVenta;
    private String tipoMoneda;
    private Cliente cliente;
    private Trabajador trabajador;

    public Venta() {
    }

    public Venta(int id, String estadoVenta, int numeroClienteVenta, int codigoProductoVenta, double iva, String tipoPago, Date fechaVenta, String tipoMoneda, Cliente cliente, Trabajador trabajador) {
        this.id = id;
        this.estadoVenta = estadoVenta;
        this.numeroClienteVenta = numeroClienteVenta;
        this.codigoProductoVenta = codigoProductoVenta;
        this.iva = iva;
        this.tipoPago = tipoPago;
        this.fechaVenta = fechaVenta;
        this.tipoMoneda = tipoMoneda;
        this.cliente = cliente;
        this.trabajador = trabajador;
    }
    public Venta( String estadoVenta, int numeroClienteVenta, int codigoProductoVenta, double iva, String tipoPago, Date fechaVenta, String tipoMoneda, Cliente cliente, Trabajador trabajador) {
        this.estadoVenta = estadoVenta;
        this.numeroClienteVenta = numeroClienteVenta;
        this.codigoProductoVenta = codigoProductoVenta;
        this.iva = iva;
        this.tipoPago = tipoPago;
        this.fechaVenta = fechaVenta;
        this.tipoMoneda = tipoMoneda;
        this.cliente = cliente;
        this.trabajador = trabajador;
    }


    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "estado_venta")
    public String getEstadoVenta() {
        return estadoVenta;
    }

    public void setEstadoVenta(String estadoVenta) {
        this.estadoVenta = estadoVenta;
    }

    @Basic
    @Column(name = "numero_cliente_venta")
    public int getNumeroClienteVenta() {
        return numeroClienteVenta;
    }

    public void setNumeroClienteVenta(int numeroClienteVenta) {
        this.numeroClienteVenta = numeroClienteVenta;
    }

    @Basic
    @Column(name = "codigo_producto_venta")
    public int getCodigoProductoVenta() {
        return codigoProductoVenta;
    }

    public void setCodigoProductoVenta(int codigoProductoVenta) {
        this.codigoProductoVenta = codigoProductoVenta;
    }

    @Basic
    @Column(name = "iva")
    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    @Basic
    @Column(name = "tipo_pago")
    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    @Basic
    @Column(name = "fecha_venta")
    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    @Basic
    @Column(name = "tipo_moneda")
    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return id == venta.id &&
                numeroClienteVenta == venta.numeroClienteVenta &&
                codigoProductoVenta == venta.codigoProductoVenta &&
                Double.compare(venta.iva, iva) == 0 &&
                Objects.equals(estadoVenta, venta.estadoVenta) &&
                Objects.equals(tipoPago, venta.tipoPago) &&
                Objects.equals(fechaVenta, venta.fechaVenta) &&
                Objects.equals(tipoMoneda, venta.tipoMoneda);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, estadoVenta, numeroClienteVenta, codigoProductoVenta, iva, tipoPago, fechaVenta, tipoMoneda);
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_trabajador", referencedColumnName = "id")
    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }


    @Override
    public String toString() {
        return "Codigo de Producto: " + codigoProductoVenta +
                ", Tipo Moneda: "+tipoMoneda+
                ", Cliente: " + cliente +
                ", Trabajador=" + trabajador;
    }
}
