package com.marioea.farmaciapaladium.incidencias;

import com.marioea.farmaciapaladium.admin.AdminScreen;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Clase que envia incidencias ocurridas durante el uso del programa
 */
public class Incidencias {
    /**
     * Metodo que envia un email al correo electronico asignado con el contenido de la incidencia
     * @param destinatario
     * @param asunto
     * @param cuerpo
     */
    public void enviarMail(String destinatario, String asunto, String cuerpo) {

        String remitente = "escosmario@gmail.com"; // Direccion del remitente

        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com"); // El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.clave", "mariolongares1"); // Password cuenta
        props.put("mail.smtp.auth", "true"); // Autenticación mediante usuario y password
        props.put("mail.smtp.starttls.enable", "true"); // Conexion segura al servidor
        props.put("mail.smtp.port", "587"); // Puerto SMTP seguro de Google

        //creamos una sesion con instancia por defecto de las properties
        Session session = Session.getDefaultInstance(props);
        //creamos un mimemessage pasandole la sesion
        MimeMessage message = new MimeMessage(session);

        try {
            //enviamos el mensaje desde la direccion del remitente
            message.setFrom(new InternetAddress(remitente));
            message.addRecipients(Message.RecipientType.TO, destinatario);
            //cogemos el asunto del mensaje
            message.setSubject(asunto);
            //cogemos el cuerpo del mensaje en formato de html
            message.setText(cuerpo, null, "html");
            message.setText(cuerpo);
            //establecemos el medio de transporte mediante el puerto smtp
            Transport transport = session.getTransport("smtp");
            //conectamos con el puerto seguro de google el smtp y añadimos la password
            transport.connect("smtp.gmail.com", remitente, "mariolongares1");
            //enviamos el mensaje
            transport.sendMessage(message, message.getAllRecipients());
            //cerramos el medio de transporte
            transport.close();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }


}
