package com.marioea.farmaciapaladium.vendedor;

import com.github.lgooddatepicker.components.DatePicker;
import com.marioea.farmaciapaladium.enumeraciones.EnumeracionEstadoVenta;
import com.marioea.farmaciapaladium.enumeraciones.EnumeracionTipoMoneda;
import com.marioea.farmaciapaladium.enumeraciones.EnumeracionTipoPago;

import javax.swing.*;
import java.awt.*;

/**
 * Esta ventana corresponde al usuario con un permiso de vendedor
 * En ella se almacenaran las ventas y tambien se mandaran indicencias mediante el correo electronico
 */
public class VendedorScreen extends JFrame{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JComboBox comboEstadoVentaVendedor;
    public JTextField txtNumeroClienteVentaVendedor;
    public JTextField txtCodigoProductoVentaVendedor;
    public JTextField txtIvaVentaVendedor;
    public JComboBox comboTipoPagoVentaVendedor;
    public DatePicker dateFechaVentaVendedor;
    public JComboBox comboTipoMonedaVentaVendedor;
    public JComboBox comboTrabajadorVendedor;
    public JButton generarVentaVendedorbtn;
    public JButton modificarVentaVendedorbtn;
    public JButton eliminarVentaVendedorbtn;
    public JList listVentaVendedor;
    public JComboBox comboClienteVendedor;
    public JButton generarPDFVentaVendedorbtn;
    public JButton generarExcelVentaVendedorbtn;
    public JButton regresoVentasbtn;
    public JTextField txtDestinatarioVendedor;
    public JTextField txtAsuntoVendedor;
    public JButton enviarMailVendedorbtn;
    public JTextArea textAreaMensajeVendedor;
    public JButton generarCodigosBarrasVendedorbtn;
    public JButton buscarVentaPorTipobtn;
    public JTextField txtBuscarVentaVendedor;
    public JMenuItem acercaDeItem;
    public JMenuItem ayudaItem;

    public  DefaultListModel dlmVenta;

    /**
     * Constructor de la clase vendedor
     * Donde se marca datos de la ventana como el tamaño, nombre de la ventana
     * y se da funcionamiento al menu de la parte superior y las distintas enumeraciones de la clase venta
     */
    public VendedorScreen(){
        this.setTitle("Vendedor");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("logo.png"));
        this.setIconImage(ico);
        crearMenu();


        crearModelos();
        enumeracionEstadoVentaVendedor();
        enumeracionTipoMonedaVendedor();
        enumeracionTipoPagoVendedor();


    }

    /**
     * Barra de menu que se genera en la parte superior de la ventana
     * Incluye datos como el acerca de y la ayuda
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");

        acercaDeItem = new JMenuItem("Acerca de");
        acercaDeItem.setActionCommand("Acerca de Vendedor");
        ayudaItem = new JMenuItem("Ayuda");
        ayudaItem.setActionCommand("Ayuda Vendedor");


        menu.add(acercaDeItem);
        menu.add(ayudaItem);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
    /**
     * Le damos a las listar creadas un default list model
     */
    private void crearModelos() {
        dlmVenta = new DefaultListModel();
        listVentaVendedor.setModel(dlmVenta);



    }

    /**
     * Metodo que coge los datos de la clase enumerada del estado de venta del vendedor
     * y los almacena en una combobox
     */
    public void enumeracionEstadoVentaVendedor(){
        for (EnumeracionEstadoVenta enumeracionEstadoVenta: EnumeracionEstadoVenta.values()){
            comboEstadoVentaVendedor.addItem(enumeracionEstadoVenta.getEstadoVenta());
        }
        comboEstadoVentaVendedor.setSelectedItem(-1);
    }

    /**
     * Metodo que coge los datos de la clase enumerada de tipo de pago del vendedor
     * y los almacena en una combobox
     */
    public void enumeracionTipoPagoVendedor(){
        for (EnumeracionTipoPago enumeracionTipoPago: EnumeracionTipoPago.values()){
            comboTipoPagoVentaVendedor.addItem(enumeracionTipoPago.getTipoPago());
        }
        comboTipoPagoVentaVendedor.setSelectedItem(-1);
    }

    /**
     * Metodo que coge los datos de la clase enumerada de tipo de moneda del vendedor
     * y los almacena en una combobox
     */
    public void enumeracionTipoMonedaVendedor(){
        for (EnumeracionTipoMoneda enumeracionTipoMoneda: EnumeracionTipoMoneda.values()){
            comboTipoMonedaVentaVendedor.addItem(enumeracionTipoMoneda.getTipoMoneda());
        }
        comboTipoMonedaVentaVendedor.setSelectedItem(-1);
    }

}
