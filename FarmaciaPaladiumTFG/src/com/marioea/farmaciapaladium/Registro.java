package com.marioea.farmaciapaladium;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Registro {
    private int id;
    private String nombreUsuario;
    private String passwordRegistro;
    private String tipoUsuario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_usuario")
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Basic
    @Column(name = "password_registro")
    public String getPasswordRegistro() {
        return passwordRegistro;
    }

    public void setPasswordRegistro(String passwordRegistro) {
        this.passwordRegistro = passwordRegistro;
    }

    @Basic
    @Column(name = "tipo_usuario")
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registro registro = (Registro) o;
        return id == registro.id &&
                Objects.equals(nombreUsuario, registro.nombreUsuario) &&
                Objects.equals(passwordRegistro, registro.passwordRegistro) &&
                Objects.equals(tipoUsuario, registro.tipoUsuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreUsuario, passwordRegistro, tipoUsuario);
    }


    @Override
    public String toString() {
        return "Usuario: '" + nombreUsuario + '\'' +
                ", Contraseña: '" + passwordRegistro + '\'' +
                ", Tipo de Usuario: '" + tipoUsuario + '\'';
    }
}
