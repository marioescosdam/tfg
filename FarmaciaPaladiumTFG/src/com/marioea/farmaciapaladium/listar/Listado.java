package com.marioea.farmaciapaladium.listar;

import com.marioea.farmaciapaladium.*;
import com.marioea.farmaciapaladium.admin.AdminScreen;
import com.marioea.farmaciapaladium.consultor.ConsultorScreen;
import com.marioea.farmaciapaladium.registro.LoginRegister;
import com.marioea.farmaciapaladium.vendedor.VendedorScreen;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que almacena todos los metodos de listar de la aplicacion
 */
public class Listado {
    /**
     * Metodo que se encarga de listar todos los clientes del administrador
     * @param listaClientes
     * @param admin
     */
    public void listarClientes(ArrayList<Cliente> listaClientes, AdminScreen admin){
        admin.dlmCliente.clear();
        for(Cliente cliente : listaClientes){
            admin.dlmCliente.addElement(cliente);
        }
    }

    /**
     * Metodo que se encarga de listar todos los productos del administrador
     * @param listaProductos
     * @param admin
     */
    public void listarProductos(ArrayList<Producto> listaProductos, AdminScreen admin){
        admin.dlmProducto.clear();
        for(Producto producto : listaProductos){
            admin.dlmProducto.addElement(producto);
        }
    }

    /**
     * Metodo que se encarga de listar todos los trabajadores del administrador
     * @param listaTrabajadores
     * @param admin
     */
    public void listarTrabajadores(ArrayList<Trabajador> listaTrabajadores, AdminScreen admin){
        admin.dlmTrabajador.clear();
        for(Trabajador trabajador : listaTrabajadores){
            admin.dlmTrabajador.addElement(trabajador);
        }
    }

    /**
     * Metodo que se encarga de listar todos las ventas del administrador
     * @param listaVentas
     * @param admin
     */
    public void listarVentas(ArrayList<Venta> listaVentas, AdminScreen admin){
        admin.dlmVenta.clear();
        for(Venta venta : listaVentas){
            admin.dlmVenta.addElement(venta);
        }
    }



    /**
     * Metodo que se encarga de listar todos los usuarios del administrador
     * @param listaUsuarios
     * @param admin
     */
    public void listarUsuariosAdmin(ArrayList<Registro> listaUsuarios, AdminScreen admin){
        admin.dlmRegistro.clear();
        for(Registro registro : listaUsuarios){
            admin.dlmRegistro.addElement(registro);
        }
    }

    /**
     * Metodo que se encarga de listar todos los clientes que contienen un producto del administrador
     * @param lista
     * @param admin
     */
    public void listarClientesProducto(List<Cliente> lista, AdminScreen admin){
        Producto producto = (Producto) admin.listProducto.getSelectedValue();
        admin.dlmProductoCliente.clear();

        lista.removeAll(producto.getClientes());

        for (Cliente cliente : lista) {
            admin.dlmProductoCliente.addElement(cliente);
        }
    }

    /**
     * Metodo que se encarga de listar todos los productos que contienen un cliente del administrador
     * @param admin
     */
    public void listarProductoCliente(AdminScreen admin){
        Cliente cliente = (Cliente) admin.listCliente.getSelectedValue();
        admin.dlmClienteProducto.clear();
        for(Producto producto : cliente.getProductos()){
            admin.dlmClienteProducto.addElement(producto);
        }

    }

    /**
     * Metodo que se encarga de listar todos los trabajadores que contienen un producto del administrador
     * @param lista
     * @param admin
     */
    public void listarTrabajadorProducto(List<Trabajador> lista, AdminScreen admin){
        Producto producto = (Producto) admin.listProducto.getSelectedValue();
        admin.dlmProductoTrabajador.clear();

        lista.removeAll(producto.getTrabajadores());

        for (Trabajador trabajador : lista) {
            admin.dlmProductoTrabajador.addElement(trabajador);
        }

    }

    /**
     *  Metodo que se encarga de listar todos los productos que contienen un trabajador del administrador
     * @param admin
     */
    public void listarProductoTrabajador(AdminScreen admin){
        Trabajador trabajador = (Trabajador) admin.listTrabajador.getSelectedValue();
        admin.dlmTrabajadorProducto.clear();
        for(Producto producto : trabajador.getProductos()){
            admin.dlmTrabajadorProducto.addElement(producto);
        }

    }


    /**
     * Metodo que se encarga de listar todos los clientes del consultor
     * @param listaClientesConsultor
     * @param consultor
     */
    public void listarClientesConsultor(ArrayList<Cliente> listaClientesConsultor, ConsultorScreen consultor){
        consultor.dlmCliente.clear();
        for(Cliente cliente : listaClientesConsultor){
            consultor.dlmCliente.addElement(cliente);
        }
    }

    /**
     * Metodo que se encarga de listar todos los productos del consultor
     * @param listaProductosConsultor
     * @param consultor
     */
    public void listarProductosConsultor(ArrayList<Producto> listaProductosConsultor, ConsultorScreen consultor){
        consultor.dlmProducto.clear();
        for(Producto producto : listaProductosConsultor){
            consultor.dlmProducto.addElement(producto);
        }
    }

    /**
     * Metodo que se encarga de listar todos los trabajadores del consultor
     * @param listaTrabajadoresConsultor
     * @param consultor
     */
    public void listarTrabajadoresConsultor(ArrayList<Trabajador> listaTrabajadoresConsultor, ConsultorScreen consultor){
        consultor.dlmTrabajador.clear();
        for(Trabajador trabajador : listaTrabajadoresConsultor){
            consultor.dlmTrabajador.addElement(trabajador);
        }
    }

    /**
     * Metodo que se encarga de listar todos las ventas del consultor
     * @param listaVentasConsultor
     * @param consultor
     */
    public void listarVentasConsultor(ArrayList<Venta> listaVentasConsultor, ConsultorScreen consultor){
        consultor.dlmVenta.clear();
        for(Venta venta : listaVentasConsultor){
            consultor.dlmVenta.addElement(venta);
        }
    }

    /**
     * Metodo que se encarga de listar todos los productos que contienen un cliente del consultor
     * @param consultor
     */
    public void listarProductoClienteConsultor(ConsultorScreen consultor){
        Cliente cliente = (Cliente) consultor.listClientesConsultor.getSelectedValue();
        consultor.dlmClienteProducto.clear();
        for(Producto producto : cliente.getProductos()){
            consultor.dlmClienteProducto.addElement(producto);
        }

    }

    /**
     * Metodo que se encarga de listar todos los productos que contienen un trabajador del consultor
     * @param consultor
     */
    public void listarProductoTrabajadorConsultor(ConsultorScreen consultor){
        Trabajador trabajador = (Trabajador) consultor.listTrabajadorConsultor.getSelectedValue();
        consultor.dlmTrabajadorProducto.clear();
        for(Producto producto : trabajador.getProductos()){
            consultor.dlmTrabajadorProducto.addElement(producto);
        }

    }

    /**
     * Metodo que se encarga de listar todos las ventas del vendedor
     * @param listaVentas
     * @param vendedor
     */
    public void listarVentasVendedor(ArrayList<Venta> listaVentas, VendedorScreen vendedor){
        vendedor.dlmVenta.clear();
        for(Venta venta : listaVentas){
            vendedor.dlmVenta.addElement(venta);
        }
    }
}
