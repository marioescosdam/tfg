import com.marioea.farmaciapaladium.gui.Controlador;
import com.marioea.farmaciapaladium.registro.LoginScreen;
import com.marioea.farmaciapaladium.gui.Modelo;


public class Main {
    public static void main(String[] args) {
        LoginScreen login = new LoginScreen();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(login,modelo);
    }
}