package com.marioea.farmaciapaladium.gui;

import com.marioea.farmaciapaladium.*;

import com.marioea.farmaciapaladium.admin.AdminScreen;

import com.marioea.farmaciapaladium.codigosbarras.CodigoBarras;
import com.marioea.farmaciapaladium.consultor.ConsultorScreen;
import com.marioea.farmaciapaladium.incidencias.Incidencias;
import com.marioea.farmaciapaladium.listar.Listado;
import com.marioea.farmaciapaladium.pdf.GenerarPDF;

import com.marioea.farmaciapaladium.pdf.GenerarXLSX;
import com.marioea.farmaciapaladium.registro.LoginRegister;
import com.marioea.farmaciapaladium.registro.LoginScreen;
import com.marioea.farmaciapaladium.resets.Resets;
import com.marioea.farmaciapaladium.vendedor.VendedorScreen;
import ejercicio1.LibreriaCifrado1;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Controlador implements ActionListener, ListSelectionListener {
    Icon creador;
    private CodigoBarras codigo;
    private LoginScreen login;
    private LoginRegister register;
    private Modelo modelo;
    private GenerarPDF pdf;
    private AdminScreen admin;
    private ConsultorScreen consultor;
    private VendedorScreen vendedor;
    private GenerarXLSX excel;
    private Incidencias incidencias;
    private Resets resets;
    private Listado listado;
    final String clave = "1111";


    /**
     * Constructor de controlador donde se inicializan las instancias a todas las clases, metodos de listar
     * y la comprobacion de la existencia de los usuarios
     * @param login
     * @param modelo
     */
    public Controlador(LoginScreen login, Modelo modelo) {
        listado=new Listado();
        resets=new Resets();
        incidencias=new Incidencias();
        codigo=new CodigoBarras();
        register=new LoginRegister();
        excel=new GenerarXLSX();
        vendedor=new VendedorScreen();
        creador=new ImageIcon("iconofoto\\iconocreador.jpg");
        pdf=new GenerarPDF();
        admin=new AdminScreen();
        consultor=new ConsultorScreen();
        this.login = login;
        this.modelo = modelo;
        modelo.conectar();
        listado.listarClientes(modelo.getCliente(),admin);
        listado.listarProductos(modelo.getProducto(),admin);
        listado.listarTrabajadores(modelo.getTrabajador(),admin);
        listado.listarVentas(modelo.getVenta(),admin);
        listado.listarUsuariosAdmin(modelo.getRegistro(),admin);
        listado.listarVentasVendedor(modelo.getVenta(),vendedor);
        if(!modelo.comprobarExistenciaUsuario()){
            register.setVisible(true);

        }else{
            login.setVisible(true);
        }



        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * Metodo del action performed para darle una funcionalidad a cada boton de las distintas vistas
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            case "Acerca de":

                JOptionPane.showMessageDialog(null,"Programador y Diseñador de la Aplicacion: \n"
                        +"MARIO ESCOS ALIAGA","Creador de la APP",JOptionPane.PLAIN_MESSAGE,creador);
                break;
            case "Ayuda":
                try{
                    int opcion=0;
                    String op="";
                    op=JOptionPane.showInputDialog("***MENU DE AYUDA*** \n"
                            +"INTRODUCE EL NUMERO DE USUARIO DEL QUE DESEAS SABER: \n"
                            + "1.Administrador\n"
                            +"2.Consultor\n"
                            +"3.Vendedor");
                    opcion=Integer.parseInt(op);
                    switch (opcion){
                        case 1:
                            try {
                                String url="manuales\\ManualAdmin.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                            break;
                        case 2:
                            try {
                                String url="manuales\\ManualConsultor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            break;
                        case 3:
                            try {
                                String url="manuales\\ManualVendedor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }


                            break;

                    }
                }catch (NumberFormatException ex){
                    JOptionPane.showMessageDialog(null, "Introduce un numero");
                }


                break;
            case "Acerca de Vendedor":

                JOptionPane.showMessageDialog(null,"Programador y Diseñador de la Aplicacion(Vendedor): \n"
                        +"MARIO ESCOS ALIAGA","Creador de la APP",JOptionPane.PLAIN_MESSAGE,creador);
                break;
            case "Ayuda Vendedor":
                try{
                    int opcion=0;
                    String op="";
                    op=JOptionPane.showInputDialog("***MENU DE AYUDA VENDEDOR*** \n"
                            +"INTRODUCE EL NUMERO DE USUARIO DEL QUE DESEAS SABER: \n"
                            + "1.Administrador\n"
                            +"2.Consultor\n"
                            +"3.Vendedor");
                    opcion=Integer.parseInt(op);
                    switch (opcion){
                        case 1:
                            try {
                                String url="manuales\\ManualAdmin.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                            break;
                        case 2:
                            try {
                                String url="manuales\\ManualConsultor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            break;
                        case 3:
                            try {
                                String url="manuales\\ManualVendedor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }


                            break;

                    }
                }catch (NumberFormatException ex){
                    JOptionPane.showMessageDialog(null, "Introduce un numero");
                }


                break;
            case "Acerca de Consultor":

                JOptionPane.showMessageDialog(null,"Programador y Diseñador de la Aplicacion(Consultor): \n"
                        +"MARIO ESCOS ALIAGA","Creador de la APP",JOptionPane.PLAIN_MESSAGE,creador);
                break;
            case "Ayuda Consultor":
                try{
                    int opcion=0;
                    String op="";
                    op=JOptionPane.showInputDialog("***MENU DE AYUDA CONSULTOR*** \n"
                            +"INTRODUCE EL NUMERO DE USUARIO DEL QUE DESEAS SABER: \n"
                            + "1.Administrador\n"
                            +"2.Consultor\n"
                            +"3.Vendedor");
                    opcion=Integer.parseInt(op);
                    switch (opcion){
                        case 1:
                            try {
                                String url="manuales\\ManualAdmin.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                            break;
                        case 2:
                            try {
                                String url="manuales\\ManualConsultor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            break;
                        case 3:
                            try {
                                String url="manuales\\ManualVendedor.txt";
                                ProcessBuilder p=new ProcessBuilder();
                                p.command("cmd.exe","/c",url);
                                p.start();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }


                            break;

                    }
                }catch (NumberFormatException ex){
                    JOptionPane.showMessageDialog(null, "Introduce un numero");
                }


                break;
            case "Login":
                String usuario=login.txtUser.getText();
                String password=login.txtPassword.getText();

                String permisos= String.valueOf(login.comboTipoUsuario.getSelectedItem());

                boolean comprobacion= false;
                try {
                    comprobacion = modelo.comprobante(usuario, LibreriaCifrado1.obtenerHash(password),permisos);
                    if(comprobacion){
                        if(login.comboTipoUsuario.getSelectedItem().equals("Administrador")){

                            admin.setVisible(true);
                            login.setVisible(false);
                            login.dispose();

                        }else if(login.comboTipoUsuario.getSelectedItem().equals("Consultor")){

                            consultor.setVisible(true);
                            login.setVisible(false);
                            login.dispose();
                        }else if(login.comboTipoUsuario.getSelectedItem().equals("Vendedor")){

                            vendedor.setVisible(true);
                            login.setVisible(false);
                            login.dispose();
                        }


                        login.txtUser.setText(null);
                        login.txtPassword.setText(null);

                    }else {
                        JOptionPane.showMessageDialog(null, "Usuario o Contraseña Erroneos");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                listado.listarUsuariosAdmin(modelo.getRegistro(),admin);
                break;
            case "Reset":
                login.txtUser.setText(null);
                login.txtPassword.setText(null);
                login.comboTipoUsuario.setSelectedIndex(-1);
                break;

            case "Nuevo Usuario":
                if (!register.txtNuevoUsuario.getText().equals("")&&!register.txtNuevaPassword.getText().equals("") && register.comboTiposUsuario.getSelectedItem() != null){
                Registro altaRegistro = new Registro();
                altaRegistro.setNombreUsuario(register.txtNuevoUsuario.getText());
                    try {
                        altaRegistro.setPasswordRegistro(LibreriaCifrado1.obtenerHash(register.txtNuevaPassword.getText()));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    altaRegistro.setTipoUsuario(String.valueOf(register.comboTiposUsuario.getSelectedItem()));
                modelo.altaUsuario(altaRegistro);
                resets.resetRegistro(register);
            }else{
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                }


                login.setVisible(true);
                register.setVisible(false);
                register.dispose();

                break;




            case "Agregar Usuario":
                if (!admin.txtNuevoUsuarioAdmin.getText().equals("")&&!admin.txtNuevaPasswordAdmin.getText().equals("") && admin.comboTiposUsuario.getSelectedItem() != null){
                    Registro altaRegistro = new Registro();
                    altaRegistro.setNombreUsuario(admin.txtNuevoUsuarioAdmin.getText());
                    try {
                        altaRegistro.setPasswordRegistro(LibreriaCifrado1.obtenerHash(admin.txtNuevaPasswordAdmin.getText()));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    altaRegistro.setTipoUsuario(String.valueOf(admin.comboTiposUsuario.getSelectedItem()));
                    modelo.altaUsuario(altaRegistro);
                    resets.resetRegistroAdmin(admin);
                }else{
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarUsuariosAdmin(modelo.getRegistro(),admin);

                break;

            case "Modificar Campos Usuario":
                if (admin.listNuevoUsuarioAdmin.getSelectedValue() != null){
                    Registro modificarRegistro= (Registro) admin.listNuevoUsuarioAdmin.getSelectedValue();
                    modificarRegistro.setNombreUsuario(admin.txtNuevoUsuarioAdmin.getText());
                    modificarRegistro.setPasswordRegistro(admin.txtNuevaPasswordAdmin.getText());
                    modificarRegistro.setTipoUsuario(String.valueOf(admin.comboTiposUsuario.getSelectedItem()));
                    modelo.modificarUsuario(modificarRegistro);
                    resets.resetRegistroAdmin(admin);
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun usuario","Ningun Usuario Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarUsuariosAdmin(modelo.getRegistro(),admin);
                admin.txtNuevaPasswordAdmin.setEnabled(true);
                break;

            case "Eliminar Usuario Registro":
                if (admin.listNuevoUsuarioAdmin.getSelectedValue() != null){
                    Registro eliminarRegistro = (Registro) admin.listNuevoUsuarioAdmin.getSelectedValue();
                    modelo.eliminarUsuario(eliminarRegistro);
                    resets.resetRegistroAdmin(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun usuario","Ningun Usuario Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarUsuariosAdmin(modelo.getRegistro(),admin);
                break;
            case "RegresoAdmin":
                admin.setVisible(false);
                login.setVisible(true);
                admin.dispose();
                if(!modelo.comprobarExistenciaUsuario()){
                    register.setVisible(true);

                }else{
                    login.setVisible(true);
                }
                break;

                //Cliente
            case "Alta del Cliente":
                if (!admin.txtNombreCliente.getText().equals("") && !admin.txtApellidoCliente.getText().equals("") && !admin.txtTelefonoCliente.getText().equals("") && comprobanteEmail(admin.txtEmailCliente.getText())&& comprobanteDni(admin.txtDniCliente.getText()) && !admin.txtDireccionCliente.getText().equals("") && admin.comboEstadoCliente.getSelectedItem() != null && comprobanteValorNumerico(admin.txtNumeroCliente.getText())){
                    try {
                    Cliente altaCliente = new Cliente();
                    altaCliente.setNombreCliente(admin.txtNombreCliente.getText());
                    altaCliente.setApellidosCliente(admin.txtApellidoCliente.getText());
                    altaCliente.setTelefono(LibreriaCifrado1.encriptar(admin.txtTelefonoCliente.getText(),clave));
                    altaCliente.setEmail(admin.txtEmailCliente.getText());
                    altaCliente.setDni( LibreriaCifrado1.encriptar(admin.txtDniCliente.getText(),clave));
                    altaCliente.setDireccion(LibreriaCifrado1.encriptar(admin.txtDireccionCliente.getText(),clave));
                    altaCliente.setEstado(String.valueOf(admin.comboEstadoCliente.getSelectedItem()));
                    altaCliente.setNumeroCliente(Integer.parseInt(admin.txtNumeroCliente.getText()));
                    modelo.altaCliente(altaCliente);
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchAlgorithmException ex) {
                        ex.printStackTrace();
                    } catch (InvalidKeyException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchPaddingException ex) {
                        ex.printStackTrace();
                    } catch (IllegalBlockSizeException ex) {
                        ex.printStackTrace();
                    } catch (BadPaddingException ex) {
                        ex.printStackTrace();
                    }
                    resets.resetCliente(admin);
                }else {
                    if (!comprobanteValorNumerico(admin.txtNumeroCliente.getText())){
                        JOptionPane.showMessageDialog(null,"El numero del Cliente no es un valor numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                    }else if(!comprobanteEmail(admin.txtEmailCliente.getText())){
                        JOptionPane.showMessageDialog(null,"El email del Cliente no contiene los caracteres propios de un email","Email Incorrecto",JOptionPane.WARNING_MESSAGE);
                    }else if (!comprobanteDni(admin.txtDniCliente.getText())){
                        JOptionPane.showMessageDialog(null,"El dni es erroneo comprueba que hay numeros y letras","Email Incorrecto",JOptionPane.WARNING_MESSAGE);
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                    }
                }
                listado.listarClientes(modelo.getCliente(),admin);
                break;
            case "Modificar un Cliente":
                if (admin.listCliente.getSelectedValue() != null){
                    Cliente modificarCliente = (Cliente) admin.listCliente.getSelectedValue();
                    modificarCliente.setNombreCliente(admin.txtNombreCliente.getText());
                    modificarCliente.setApellidosCliente(admin.txtApellidoCliente.getText());
                    modificarCliente.setTelefono(admin.txtTelefonoCliente.getText());
                    modificarCliente.setEmail(admin.txtEmailCliente.getText());
                    modificarCliente.setDni(admin.txtDniCliente.getText());
                    modificarCliente.setDireccion(admin.txtDireccionCliente.getText());
                    modificarCliente.setEstado(String.valueOf(admin.comboEstadoCliente.getSelectedItem()));
                    modificarCliente.setNumeroCliente(Integer.parseInt(admin.txtNumeroCliente.getText()));
                    modelo.modificarCliente(modificarCliente);
                    resets.resetCliente(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun cliente","Ningun Cliente Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarClientes(modelo.getCliente(),admin);
                break;
            case "Buscar Cliente":
                String nombre=admin.txtBuscarCliente.getText();
                admin.txtBuscarCliente.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (admin.txtBuscarCliente.getText().length()==0){
                            listado.listarClientes(modelo.getCliente(),admin);
                        }
                    }
                });
                if(!nombre.isEmpty()){
                    listado.listarClientes(modelo.getClienteEstado(nombre),admin);
                }
                break;
            case "Eliminar un Cliente":
                if (admin.listCliente.getSelectedValue() != null){
                    Cliente eliminarCliente = (Cliente) admin.listCliente.getSelectedValue();
                    modelo.eliminarCliente(eliminarCliente);
                    resets.resetCliente(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun cliente","Ningun Cliente Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarClientes(modelo.getCliente(),admin);
                break;
            case "Generar PDF Cliente":
                try {
                    pdf.generarpdfCliente(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Cliente Generado");
                break;

            case "Generar EXCEL Cliente":
                try {
                    excel.generarXLSXCliente(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Cliente Generado");
                break;
            case "Diagrama Cliente":
                try {
                    pdf.generarpdfClienteDiagrama(modelo);
                    excel.generarXLSXClienteDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y EXCEL del Diagrama Cliente Generado");
                break;



                //Producto
            case "Alta del Producto":

                    try {
                        if (comprobanteValorNumerico(admin.txtCodigoProducto.getText()) && !admin.txtNombreProducto.getText().equals("") && !admin.txtLaboratorio.getText().equals("") && admin.comboEstadoProducto.getSelectedItem() != null && admin.comboTipoProducto.getSelectedItem() != null && comprobanteValorNumerico(admin.txtPrecioProducto.getText()) && !admin.datePickerProducto.getText().equals("")){
                            Producto altaProducto = new Producto();
                        altaProducto.setCodigoProducto(Integer.parseInt(admin.txtCodigoProducto.getText()));
                    altaProducto.setNombre(admin.txtNombreProducto.getText());
                    altaProducto.setLaboratorio(LibreriaCifrado1.encriptar(admin.txtLaboratorio.getText(),clave));
                    altaProducto.setEstado(String.valueOf(admin.comboEstadoProducto.getSelectedItem()));
                    altaProducto.setTipo(String.valueOf(admin.comboTipoProducto.getSelectedItem()));
                    altaProducto.setStock(LibreriaCifrado1.encriptar(admin.txtStock.getText(),clave));
                    altaProducto.setPrecio(Double.parseDouble(admin.txtPrecioProducto.getText()));
                    altaProducto.setFechaFabricacion(Date.valueOf(admin.datePickerProducto.getDate()));
                    modelo.altaProducto(altaProducto);
                        }else {
                            if (!comprobanteValorNumerico(admin.txtCodigoProducto.getText())|| !comprobanteValorNumerico(admin.txtPrecioProducto.getText())){
                                JOptionPane.showMessageDialog(null,"El codigo del Producto o el precio no es un valor numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                            }else{
                                JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                            }
                        }
                       resets.resetProducto(admin);

                        listado.listarProductos(modelo.getProducto(),admin);
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchAlgorithmException ex) {
                        ex.printStackTrace();
                    } catch (InvalidKeyException ex) {
                        ex.printStackTrace();
                    } catch (NoSuchPaddingException ex) {
                        ex.printStackTrace();
                    } catch (IllegalBlockSizeException ex) {
                        ex.printStackTrace();
                    } catch (BadPaddingException ex) {
                        ex.printStackTrace();
                    }

                break;
            case "Modificar un Producto":
                if (admin.listProducto.getSelectedValue() != null){
                    Producto modificarProducto = (Producto) admin.listProducto.getSelectedValue();
                    modificarProducto.setCodigoProducto(Integer.parseInt(admin.txtCodigoProducto.getText()));
                    modificarProducto.setNombre(admin.txtNombreProducto.getText());
                    modificarProducto.setLaboratorio(admin.txtLaboratorio.getText());
                    modificarProducto.setEstado(String.valueOf(admin.comboEstadoProducto.getSelectedItem()));
                    modificarProducto.setTipo(String.valueOf(admin.comboTipoProducto.getSelectedItem()));
                    modificarProducto.setStock(admin.txtStock.getText());
                    modificarProducto.setPrecio(Double.parseDouble(admin.txtPrecioProducto.getText()));
                    modificarProducto.setFechaFabricacion(Date.valueOf(admin.datePickerProducto.getDate()));

                    modelo.modificarProducto(modificarProducto);
                    resets.resetProducto(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun producto","Ningun Producto Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarProductos(modelo.getProducto(),admin);
                break;
            case "Buscar Producto":
                if (comprobanteValorNumerico(admin.txtBuscarproductoAdmin.getText())) {
                    int productoBuscado = Integer.parseInt(admin.txtBuscarproductoAdmin.getText());
                    admin.txtBuscarproductoAdmin.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            if (admin.txtBuscarproductoAdmin.getText().length() == 0) {
                                listado.listarProductos(modelo.getProducto(), admin);
                            }
                        }
                    });
                    if (!String.valueOf(productoBuscado).isEmpty()) {
                        listado.listarProductos(modelo.getProductoCodigo(productoBuscado), admin);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"El dato no es numerico","Dato no Numerico",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "Eliminar un Producto":
                if (admin.listProducto.getSelectedValue() != null){
                    Producto eliminarProducto = (Producto) admin.listProducto.getSelectedValue();
                    modelo.eliminarProducto(eliminarProducto);
                    resets.resetProducto(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun producto","Ningun Producto Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarProductos(modelo.getProducto(),admin);
                break;
            case "Generar PDF Producto":
                try {
                    pdf.generarpdfProducto(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Producto Generado");
                break;
            case "Generar EXCEL Producto":
                try {
                    excel.generarXLSXProducto(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Producto Generado");
                break;
            case "Diagrama Producto":
                try {
                    pdf.generarpdfProductoDiagrama(modelo);
                    excel.generarXLSXProductoDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama Producto Generado");
                break;
            case "Generar Codigos de Barras":
                try {
                    codigo.GenerarCodigoBaras(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Codigos de Producto Generado");
                break;

                //Trabajador
            case "Alta del Trabajador":
                try {
                if (!admin.txtNombreTrabajador.getText().equals("") && !admin.txtApellidosTrabajador.getText().equals("") && comprobanteDni(admin.txtDniTrabajador.getText()) && comprobanteValorNumerico(admin.txtSueldoTrabajador.getText()) && !admin.txtDiasTrabajados.getText().equals("") && !admin.txtTelefonoTrabajador.getText().equals("") && !admin.txtDireccionTrabajador.getText().equals("") && admin.comboCargoTrabajador.getSelectedItem() != null){
                    Trabajador altaTrabajador = new Trabajador();
                    altaTrabajador.setNombreTrabajador(admin.txtNombreTrabajador.getText());
                    altaTrabajador.setApellidosTrabajador(admin.txtApellidosTrabajador.getText());
                    altaTrabajador.setDniTrabajador(LibreriaCifrado1.encriptar(admin.txtDniTrabajador.getText(),clave));
                    altaTrabajador.setSueldoTrabajador(Double.parseDouble(admin.txtSueldoTrabajador.getText()));
                    altaTrabajador.setDiasTrabajados(admin.txtDiasTrabajados.getText());
                    altaTrabajador.setTelefonoTrabajador(LibreriaCifrado1.encriptar(admin.txtTelefonoTrabajador.getText(),clave));
                    altaTrabajador.setDireccionTrabajador(LibreriaCifrado1.encriptar(admin.txtDireccionTrabajador.getText(),clave));
                    altaTrabajador.setCargoTrabajador(String.valueOf(admin.comboCargoTrabajador.getSelectedItem()));
                    modelo.altaTrabajador(altaTrabajador);
                    resets.resetTrabajador(admin);
                }else {
                    if (!comprobanteValorNumerico(admin.txtSueldoTrabajador.getText())){
                        JOptionPane.showMessageDialog(null,"El sueldo del Trabajador no es un valor numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                    }else if (!comprobanteDni(admin.txtDniTrabajador.getText())){
                        JOptionPane.showMessageDialog(null,"El dni es erroneo comprueba que hay numeros y letras","Email Incorrecto",JOptionPane.WARNING_MESSAGE);
                    } else{
                        JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                    }
                }
                listado.listarTrabajadores(modelo.getTrabajador(),admin);
                } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            } catch (InvalidKeyException ex) {
                ex.printStackTrace();
            } catch (NoSuchPaddingException ex) {
                ex.printStackTrace();
            } catch (IllegalBlockSizeException ex) {
                ex.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            }
                break;
            case "Modificar un Trabajador":
                if (admin.listTrabajador.getSelectedValue() != null){
                    Trabajador modificarTrabajador = (Trabajador) admin.listTrabajador.getSelectedValue();
                    modificarTrabajador.setNombreTrabajador(admin.txtNombreTrabajador.getText());
                    modificarTrabajador.setApellidosTrabajador(admin.txtApellidosTrabajador.getText());
                    modificarTrabajador.setDniTrabajador(admin.txtDniTrabajador.getText());
                    modificarTrabajador.setSueldoTrabajador(Double.parseDouble(admin.txtSueldoTrabajador.getText()));
                    modificarTrabajador.setDiasTrabajados(admin.txtDiasTrabajados.getText());
                    modificarTrabajador.setTelefonoTrabajador(admin.txtTelefonoTrabajador.getText());
                    modificarTrabajador.setDireccionTrabajador(admin.txtDireccionTrabajador.getText());
                    modificarTrabajador.setCargoTrabajador(String.valueOf(admin.comboCargoTrabajador.getSelectedItem()));
                    modelo.modificarTrabajador(modificarTrabajador);
                    resets.resetTrabajador(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ningun Trabajador Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarTrabajadores(modelo.getTrabajador(),admin);
                break;

            case "Buscar Trabajador por Dias Trabajados":
                String trabajadorBuscado= admin.txtBuscarTrabajadorAdmin.getText();
                admin.txtBuscarTrabajadorAdmin.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (admin.txtBuscarTrabajadorAdmin.getText().length()==0){
                            listado.listarTrabajadores(modelo.getTrabajador(),admin);
                        }
                    }
                });
                if(!trabajadorBuscado.isEmpty()){
                    listado.listarTrabajadores(modelo.getTrabajadorDias(trabajadorBuscado),admin);
                }
                break;
            case "Eliminar un Trabajador":
                if (admin.listTrabajador.getSelectedValue() != null){
                    Trabajador eliminarTrabajador = (Trabajador) admin.listTrabajador.getSelectedValue();
                    modelo.eliminarTrabajador(eliminarTrabajador);
                    resets.resetTrabajador(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ningun Trabajador Seleccionado",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarTrabajadores(modelo.getTrabajador(),admin);
                break;
            case "Generar PDF Trabajador":
                try {
                    pdf.generarpdfTrabajador(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Trabajador Generado");
                break;
            case "Diagrama Trabajador":
                try {
                    pdf.generarpdfTrabajadorDiagrama(modelo);
                    excel.generarXLSXTrabajadorDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Trabajador Generado");
                break;
            case "Generar EXCEL Trabajador":
                try {
                    excel.generarXLSXTrabajador(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Trabajador Generado");
                break;
                //Venta
            case "Generar una Venta":
                if (admin.comboEstadoVenta.getSelectedItem() != null && comprobanteValorNumerico(admin.txtNumeroClienteVenta.getText()) && comprobanteValorNumerico(admin.txtCodigoProductoVenta.getText()) && comprobanteValorNumerico(admin.txtIvaVenta.getText()) && admin.comboTipoPagoVenta.getSelectedItem() != null && !admin.dateFechaVenta.getText().equals("") && admin.comboTrabajador.getSelectedItem() != null && admin.comboCliente.getSelectedItem() != null){
                    Venta altaVenta = new Venta();
                    altaVenta.setEstadoVenta(String.valueOf(admin.comboEstadoVenta.getSelectedItem()));
                    altaVenta.setNumeroClienteVenta(Integer.parseInt(admin.txtNumeroClienteVenta.getText()));
                    altaVenta.setCodigoProductoVenta(Integer.parseInt(admin.txtCodigoProductoVenta.getText()));
                    altaVenta.setIva(Double.parseDouble(admin.txtIvaVenta.getText()));
                    altaVenta.setTipoPago(String.valueOf(admin.comboTipoPagoVenta.getSelectedItem()));
                    altaVenta.setFechaVenta(Date.valueOf(admin.dateFechaVenta.getDate()));
                    altaVenta.setTipoMoneda(String.valueOf(admin.comboTipoMonedaVenta.getSelectedItem()));
                    altaVenta.setTrabajador((Trabajador) admin.comboTrabajador.getSelectedItem());
                    altaVenta.setCliente((Cliente) admin.comboCliente.getSelectedItem());
                    modelo.altaVenta(altaVenta);
                    resets.resetVenta(admin);
                }else {
                    if (!comprobanteValorNumerico(admin.txtNumeroClienteVenta.getText()) || !comprobanteValorNumerico(admin.txtCodigoProductoVenta.getText()) || !comprobanteValorNumerico(admin.txtIvaVenta.getText())){
                        JOptionPane.showMessageDialog(null,"El numero del Cliente, el codigo de Producto Vendido o el Iva no es un valor numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                    }
                }
                listado.listarVentas(modelo.getVenta(),admin);
                break;
            case "Modificar una Venta":
                if (admin.listVenta.getSelectedValue() != null){
                    Venta modificarVenta = (Venta) admin.listVenta.getSelectedValue();
                    modificarVenta.setEstadoVenta(String.valueOf(admin.comboEstadoVenta.getSelectedItem()));
                    modificarVenta.setNumeroClienteVenta(Integer.parseInt(admin.txtNumeroClienteVenta.getText()));
                    modificarVenta.setCodigoProductoVenta(Integer.parseInt(admin.txtCodigoProductoVenta.getText()));
                    modificarVenta.setIva(Double.parseDouble(admin.txtIvaVenta.getText()));
                    modificarVenta.setTipoPago(String.valueOf(admin.comboTipoPagoVenta.getSelectedItem()));
                    modificarVenta.setFechaVenta(Date.valueOf(admin.dateFechaVenta.getDate()));
                    modificarVenta.setTipoMoneda(String.valueOf(admin.comboTipoMonedaVenta.getSelectedItem()));
                    modificarVenta.setTrabajador((Trabajador) admin.comboTrabajador.getSelectedItem());
                    modificarVenta.setCliente((Cliente) admin.comboCliente.getSelectedItem());

                    modelo.modificarVenta(modificarVenta);
                    resets.resetVenta(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna venta","Ninguna Venta Seleccionada",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarVentas(modelo.getVenta(),admin);
                break;
            case "Eliminar una Venta":
                if (admin.listVenta.getSelectedValue() != null){
                    Venta eliminarVenta = (Venta) admin.listVenta.getSelectedValue();
                    modelo.eliminarVenta(eliminarVenta);
                    resets.resetVenta(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna venta","Ninguna Venta Seleccionada",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarVentas(modelo.getVenta(),admin);
                break;

            case "Generar Codigos Barras Venta":
                try {
                    codigo.GenerarCodigoBarasVendedor(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Codigos de Producto Generado");
                break;
            case "Generar PDF Venta":
                try {
                    pdf.generarpdfVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Venta Generado");
                break;

            case "Generar EXCEL Venta":
                try {
                    excel.generarXLSXVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Venta Generado");
                break;
            case "Diagrama Venta":
                try {
                    pdf.generarpdfVentaDiagrama(modelo);
                    excel.generarXLSXVentaDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Ventas Generado");
                break;
            case "Asignar Producto a Cliente":
                if (admin.listProducto.getSelectedValue() != null && admin.listProductoCliente.getSelectedValue() != null){
                    Producto producto = (Producto) admin.listProducto.getSelectedValue();
                    Cliente cliente = (Cliente) admin.listProductoCliente.getSelectedValue();
                    producto.getClientes().add(cliente);
                    cliente.getProductos().add(producto);
                    modelo.modificarProducto(producto);
                    modelo.modificarCliente(cliente);
                    admin.dlmProductoCliente.clear();
                    resets.resetProducto(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo te falta por seleccionar","Campos vacios",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarProductos(modelo.getProducto(),admin);
                listado.listarClientes(modelo.getCliente(),admin);
                break;
            case "Listar Cliente Producto":
                if (admin.listCliente.getSelectedValue() != null){
                    listado.listarProductoCliente(admin);
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun cliente","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "Asignar Producto a Trabajador":
                if (admin.listProducto.getSelectedValue() != null && admin.listProductoTrabajador.getSelectedValue() != null){
                    Producto producto = (Producto) admin.listProducto.getSelectedValue();
                    Trabajador trabajador = (Trabajador) admin.listProductoTrabajador.getSelectedValue();
                    producto.getTrabajadores().add(trabajador);
                    trabajador.getProductos().add(producto);
                    modelo.modificarProducto(producto);
                    modelo.modificarTrabajador(trabajador);
                    admin.dlmProductoTrabajador.clear();
                    resets.resetProducto(admin);
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo te falta por seleccionar","Campos vacios",JOptionPane.WARNING_MESSAGE);
                }
                    listado.listarProductos(modelo.getProducto(),admin);
                    listado.listarTrabajadores(modelo.getTrabajador(),admin);
                break;
            case "Listar Trabajador Producto":
                if (admin.listTrabajador.getSelectedValue() != null){
                    listado.listarProductoTrabajador(admin);
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "Buscar Venta por Tipo de Moneda":
                String ventaBuscada=admin.txtBuscarVentaAdmin.getText();
                admin.txtBuscarVentaAdmin.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (admin.txtBuscarVentaAdmin.getText().length()==0){
                            listado.listarVentas(modelo.getVenta(),admin);
                        }
                    }
                });
                if(!ventaBuscada.isEmpty()){
                    listado.listarVentas(modelo.getVentaTipoMoneda(ventaBuscada),admin);
                }
                break;
                //Consultor
            case "Listar Clientes":
                listado.listarClientesConsultor(modelo.getCliente(),consultor);
                break;
            case "Listar Productos":
                listado.listarProductosConsultor(modelo.getProducto(),consultor);
                break;
            case "Listar Trabajadores":
                listado.listarTrabajadoresConsultor(modelo.getTrabajador(),consultor);
                break;
            case "Listar Ventas":
                listado.listarVentasConsultor(modelo.getVenta(),consultor);
                break;

            case "ListarClienteProductoConsultor":
                if (consultor.listClientesConsultor.getSelectedValue() != null){
                    listado.listarProductoClienteConsultor(consultor);
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun cliente","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "ListarTrabajadorProductoConsultor":
                if (consultor.listTrabajadorConsultor.getSelectedValue() != null){
                    listado.listarProductoTrabajadorConsultor(consultor);
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "Generar PDF Cliente Consultor":
                try {
                    pdf.generarpdfCliente(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Cliente desde Consultor Generado");
                break;
            case "Generar EXCEL Cliente Consultor":
                try {
                    excel.generarXLSXCliente(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Cliente desde Consultor Generado");
                break;
            case "Generar PDF Producto Consultor":
                try {
                    pdf.generarpdfProducto(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Producto desde Consultor Generado");
                break;
            case "Generar EXCEL Producto Consultor":
                try {
                    excel.generarXLSXCliente(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Producto desde Consultor Generado");
                break;
            case "Generar PDF Trabajador Consultor":
                try {
                    pdf.generarpdfTrabajador(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Trabajador desde Consultor Generado");
                break;
            case "Generar EXCEL Trabajador Consultor":
                try {
                    excel.generarXLSXTrabajador(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Trabajador desde Consultor Generado");
                break;
            case "Generar PDF Ventas Consultor":
                try {
                    pdf.generarpdfVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Ventas desde Consultor Generado");
                break;
            case "Generar EXCEL Ventas Consultor":
                try {
                    excel.generarXLSXVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Ventas desde Consultor Generado");
                break;

            case "Diagrama Cliente Consultor":
                try {
                    pdf.generarpdfClienteDiagrama(modelo);
                    excel.generarXLSXClienteDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Cliente Generado");
                break;
            case "Diagrama Producto Consultor":
                try {
                    pdf.generarpdfProductoDiagrama(modelo);
                    excel.generarXLSXProductoDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Producto Generado");
                break;
            case "Diagrama Trabajador Consultor":
                try {
                    pdf.generarpdfTrabajadorDiagrama(modelo);
                    excel.generarXLSXTrabajadorDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Trabajadores Generado");
                break;
            case "Diagrama Venta Consultor":
                try {
                    pdf.generarpdfVentaDiagrama(modelo);
                    excel.generarXLSXVentaDiagrama(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF y Excel del Diagrama de Venta Generado");
                break;
            case "Generar Codigos Barras Consultor":
                try {
                    codigo.GenerarCodigoBaras(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Codigos de Producto desde Consultor Generado");
                break;
            case "Generar Codigos Barras Venta Consultor":
                try {
                    codigo.GenerarCodigoBarasVendedor(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Codigos de Producto desde Consultor Generado");
                break;
            case "Buscar Cliente Consultor":
                String clienteBuscadoConsultor=consultor.txtBuscarClienteConsultor.getText();
                consultor.txtBuscarClienteConsultor.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (consultor.txtBuscarClienteConsultor.getText().length()==0){
                            listado.listarClientesConsultor(modelo.getCliente(),consultor);
                        }
                    }
                });
                if(!clienteBuscadoConsultor.isEmpty()){
                    listado.listarClientesConsultor(modelo.getClienteEstado(clienteBuscadoConsultor),consultor);
                }
                break;
            case "Buscar Producto Consultor":
                if (comprobanteValorNumerico(consultor.txtBuscarProductoConsultor.getText())) {
                    int productoBuscadoConsultor = Integer.parseInt(consultor.txtBuscarProductoConsultor.getText());
                    consultor.txtBuscarProductoConsultor.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            if (consultor.txtBuscarProductoConsultor.getText().length() == 0) {
                                listado.listarProductosConsultor(modelo.getProducto(), consultor);
                            }
                        }
                    });
                    if (!String.valueOf(productoBuscadoConsultor).isEmpty()) {
                        listado.listarProductosConsultor(modelo.getProductoCodigo(productoBuscadoConsultor), consultor);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Valor no numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "Buscar Trabajador Consultor":
                String trabajadorBuscadoConsultor= consultor.txtBuscarTrabajadorConsultor.getText();
                consultor.txtBuscarTrabajadorConsultor.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (consultor.txtBuscarTrabajadorConsultor.getText().length()==0){
                            listado.listarTrabajadoresConsultor(modelo.getTrabajador(),consultor);
                        }
                    }
                });
                if(!trabajadorBuscadoConsultor.isEmpty()){
                    listado.listarTrabajadoresConsultor(modelo.getTrabajadorDias(trabajadorBuscadoConsultor),consultor);
                }
                break;
            case "Buscar Venta Consultor":
                String ventaBuscadaConsultor=consultor.txtBuscarVentaConsultor.getText();
                consultor.txtBuscarVentaConsultor.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (consultor.txtBuscarVentaConsultor.getText().length()==0){
                            listado.listarVentasConsultor(modelo.getVenta(),consultor);
                        }
                    }
                });
                if(!ventaBuscadaConsultor.isEmpty()){
                    listado.listarVentasConsultor(modelo.getVentaTipoMoneda(ventaBuscadaConsultor),consultor);
                }
              break;

            case "Boton Regreso Consultor":
                consultor.setVisible(false);
                login.setVisible(true);
                consultor.dispose();
                break;
                //VENDEDOR
            case "GenerarVentaVendedor":
                if (vendedor.comboEstadoVentaVendedor.getSelectedItem() != null && comprobanteValorNumerico(vendedor.txtNumeroClienteVentaVendedor.getText()) && comprobanteValorNumerico(vendedor.txtCodigoProductoVentaVendedor.getText()) && comprobanteValorNumerico(vendedor.txtIvaVentaVendedor.getText()) && vendedor.comboTipoPagoVentaVendedor.getSelectedItem() != null && !vendedor.dateFechaVentaVendedor.getText().equals("") && vendedor.comboTrabajadorVendedor.getSelectedItem() != null && vendedor.comboClienteVendedor.getSelectedItem() != null){
                    Venta altaVentaVendedor = new Venta();
                    altaVentaVendedor.setEstadoVenta(String.valueOf(vendedor.comboEstadoVentaVendedor.getSelectedItem()));
                    altaVentaVendedor.setNumeroClienteVenta(Integer.parseInt(vendedor.txtNumeroClienteVentaVendedor.getText()));
                    altaVentaVendedor.setCodigoProductoVenta(Integer.parseInt(vendedor.txtCodigoProductoVentaVendedor.getText()));
                    altaVentaVendedor.setIva(Double.parseDouble(vendedor.txtIvaVentaVendedor.getText()));
                    altaVentaVendedor.setTipoPago(String.valueOf(vendedor.comboTipoPagoVentaVendedor.getSelectedItem()));
                    altaVentaVendedor.setFechaVenta(Date.valueOf(vendedor.dateFechaVentaVendedor.getDate()));
                    altaVentaVendedor.setTipoMoneda(String.valueOf(vendedor.comboTipoMonedaVentaVendedor.getSelectedItem()));
                    altaVentaVendedor.setTrabajador((Trabajador) vendedor.comboTrabajadorVendedor.getSelectedItem());
                    altaVentaVendedor.setCliente((Cliente) vendedor.comboClienteVendedor.getSelectedItem());
                    modelo.altaVenta(altaVentaVendedor);
                    resets.resetVentaVendedor(vendedor);
                }else {
                    if (!comprobanteValorNumerico(vendedor.txtNumeroClienteVentaVendedor.getText()) || !comprobanteValorNumerico(vendedor.txtCodigoProductoVentaVendedor.getText()) || !comprobanteValorNumerico(vendedor.txtIvaVentaVendedor.getText())){
                        JOptionPane.showMessageDialog(null,"El numero del Cliente, el codigo de Producto Vendido o el Iva no es un valor numerico","Valor no Numerico",JOptionPane.WARNING_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Campos vacios",JOptionPane.WARNING_MESSAGE);
                    }
                }
                listado.listarVentasVendedor(modelo.getVenta(),vendedor);
                break;
            case "ModificarVentaVendedor":
                if (vendedor.listVentaVendedor.getSelectedValue() != null){
                    Venta modificarVentaVendedor = (Venta) vendedor.listVentaVendedor.getSelectedValue();
                    modificarVentaVendedor.setEstadoVenta(String.valueOf(vendedor.comboEstadoVentaVendedor.getSelectedItem()));
                    modificarVentaVendedor.setNumeroClienteVenta(Integer.parseInt(vendedor.txtNumeroClienteVentaVendedor.getText()));
                    modificarVentaVendedor.setCodigoProductoVenta(Integer.parseInt(vendedor.txtCodigoProductoVentaVendedor.getText()));
                    modificarVentaVendedor.setIva(Double.parseDouble(vendedor.txtIvaVentaVendedor.getText()));
                    modificarVentaVendedor.setTipoPago(String.valueOf(vendedor.comboTipoPagoVentaVendedor.getSelectedItem()));
                    modificarVentaVendedor.setFechaVenta(Date.valueOf(vendedor.dateFechaVentaVendedor.getDate()));
                    modificarVentaVendedor.setTipoMoneda(String.valueOf(vendedor.comboTipoMonedaVentaVendedor.getSelectedItem()));
                    modificarVentaVendedor.setTrabajador((Trabajador) vendedor.comboTrabajadorVendedor.getSelectedItem());
                    modificarVentaVendedor.setCliente((Cliente) vendedor.comboClienteVendedor.getSelectedItem());

                    modelo.modificarVenta(modificarVentaVendedor);
                    resets.resetVentaVendedor(vendedor);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna venta","Ninguna Venta Seleccionada",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarVentasVendedor(modelo.getVenta(),vendedor);
                break;
            case "EliminarVentaVendedor":
                if (vendedor.listVentaVendedor.getSelectedValue() != null){
                    Venta eliminarVentas = (Venta) vendedor.listVentaVendedor.getSelectedValue();
                    modelo.eliminarVenta(eliminarVentas);
                    resets.resetVentaVendedor(vendedor);
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna venta","Ninguna Venta Seleccionada",JOptionPane.WARNING_MESSAGE);
                }
                listado.listarVentasVendedor(modelo.getVenta(),vendedor);
                break;
            case "GenerarPDFVentaVendedor":
                try {
                    pdf.generarpdfVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "PDF de Venta desde Vendedor Generado");
                break;
            case "GenerarEXCELVentaVendedor":
                try {
                    excel.generarXLSXVenta(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "EXCEL de Venta desde Vendedor Generado");
                break;
            case"Generar Codigos Vendedor":
                try {
                    codigo.GenerarCodigoBarasVendedor(modelo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null, "Codigos de Producto en Vendedor Generado");
                break;

            case "Buscar Venta Vendedor":
                String ventaBuscadaVendedor=vendedor.txtBuscarVentaVendedor.getText();
                vendedor.txtBuscarVentaVendedor.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                        if (vendedor.txtBuscarVentaVendedor.getText().length()==0){
                            listado.listarVentasVendedor(modelo.getVenta(),vendedor);
                        }
                    }
                });
                if(!ventaBuscadaVendedor.isEmpty()){
                    listado.listarVentasVendedor(modelo.getVentaTipoMoneda(ventaBuscadaVendedor),vendedor);
                }
                break;
            case "Regreso Ventas":
                vendedor.setVisible(false);
                login.setVisible(true);
                vendedor.dispose();
                break;
            case "Enviar Mail":
                String destinatario =  admin.txtDestinatarioAdmin.getText();
                String asunto = admin.txtAsuntoAdmin.getText();
                String cuerpo = admin.textAreaMensajeAdmin.getText();
                incidencias.enviarMail(destinatario, asunto, cuerpo);
                break;
            case "Enviar Mail Consultor":
                String destinatarioConsultor =  consultor.txtDestinatarioConsultor.getText();
                String asuntoConsultor = consultor.txtAsuntoConsultor.getText();
                String cuerpoConsultor = consultor.textAreaMensajeConsultor.getText();
                incidencias.enviarMail(destinatarioConsultor, asuntoConsultor, cuerpoConsultor);
                break;
            case "Enviar Mail Vendedor":
                String destinatarioVendedor =  vendedor.txtDestinatarioVendedor.getText();
                String asuntoVendedor = vendedor.txtAsuntoVendedor.getText();
                String cuerpoVendedor = vendedor.textAreaMensajeVendedor.getText();
                incidencias.enviarMail(destinatarioVendedor, asuntoVendedor, cuerpoVendedor);
                break;

        }
        rellenarCombos();
        rellenarCombosVendedor();

    }

    /**
     * Metodo que le añade una accion a cada boton,
     * basicamente que efectuen una accion al ser pulsados
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        login.loginbtn.addActionListener(listener);
        login.resetbtn.addActionListener(listener);
        register.nuevoUsuariobtn.addActionListener(listener);

        //
        admin.altaClientebtn.addActionListener(listener);
        admin.eliminarClientebtn.addActionListener(listener);
        admin.modificarClientebtn.addActionListener(listener);
        admin.listarClienteProductobtn.addActionListener(listener);
        admin.buscarClientebtn.addActionListener(listener);


        //
        admin.altaProductobtn.addActionListener(listener);
        admin.eliminarProductobtn.addActionListener(listener);
        admin.modificarProductobtn.addActionListener(listener);
        admin.asignarProductoClientebtn.addActionListener(listener);
        admin.asignarProductoTrabajadorbtn.addActionListener(listener);
        admin.buscarProductoPorCodigobtn.addActionListener(listener);

        //
        admin.altaTrabajadorbtn.addActionListener(listener);
        admin.eliminarTrabajadorbtn.addActionListener(listener);
        admin.modificarTrabajadorbtn.addActionListener(listener);
        admin.listarTrabajadorProductobtn.addActionListener(listener);
        admin.buscarTrabajadorPorDiasbtn.addActionListener(listener);

        //
        admin.generarVentabtn.addActionListener(listener);
        admin.eliminarVentabtn.addActionListener(listener);
        admin.modificarVentabtn.addActionListener(listener);
        admin.generarCodigosBarrasbtn.addActionListener(listener);
        admin.generarCodigosBarrasVentabtn.addActionListener(listener);
        admin.buscarVentaPorTipobtn.addActionListener(listener);
        //
        admin.nuevoUsuarioAdminbtn.addActionListener(listener);
        admin.modificarUsuarioAdminbtn.addActionListener(listener);
        admin.eliminarUsuarioAdminbtn.addActionListener(listener);

        //
        admin.regreso.addActionListener(listener);
        admin.generarPDFClientebtn.addActionListener(listener);
        admin.generarPDFProductobtn.addActionListener(listener);
        admin.generarPDFTrabajadorbtn.addActionListener(listener);
        admin.generarPDFVentabtn.addActionListener(listener);
        admin.generarExcelClientebtn.addActionListener(listener);
        admin.generarExcelProductobtn.addActionListener(listener);
        admin.generarExcelTrabajadorbtn.addActionListener(listener);
        admin.generarExcelVentabtn.addActionListener(listener);
        admin.generarCodigosBarrasbtn.addActionListener(listener);
        admin.diagramaClientebtn.addActionListener(listener);
        admin.diagramaProductobtn.addActionListener(listener);
        admin.diagramaTrabajadorbtn.addActionListener(listener);
        admin.diagramaVentabtn.addActionListener(listener);
        //
        admin.acercaDeItem.addActionListener(listener);
        admin.ayudaItem.addActionListener(listener);
        admin.enviarMailAdminbtn.addActionListener(listener);


        //Consultor
        consultor.listarClientesConsultorbtn.addActionListener(listener);
        consultor.listarProductosConsultorbtn.addActionListener(listener);
        consultor.listarTrabajadorConsultorbtn.addActionListener(listener);
        consultor.listarVentasConsultorbtn.addActionListener(listener);
        consultor.listarClienteProductoConsultorbtn.addActionListener(listener);
        consultor.listarTrabajadorProductoConsultorbtn.addActionListener(listener);
        consultor.regresoConsultorbtn.addActionListener(listener);
        consultor.enviarMailConsultorbtn.addActionListener(listener);
        consultor.generarPDFClienteConsultorbtn.addActionListener(listener);
        consultor.generarExcelClientebtn.addActionListener(listener);
        consultor.generarPdfProductoConsultorbtn.addActionListener(listener);
        consultor.generarExcelProductosConsultorbtn.addActionListener(listener);
        consultor.generarPdfTrabajadorConsultorbtn.addActionListener(listener);
        consultor.generarExcelTrabajadorConsultorbtn.addActionListener(listener);
        consultor.generarPdfVentasConsultorbtn.addActionListener(listener);
        consultor.generarExcelVentasConsultorbtn.addActionListener(listener);
        consultor.diagramaClienteConsultorbtn.addActionListener(listener);
        consultor.diagramaProductoConsultorbtn.addActionListener(listener);
        consultor.diagramaTrabajadorConsultorbtn.addActionListener(listener);
        consultor.diagramaVentaConsultorbtn.addActionListener(listener);
        consultor.generarCodigosBarrasConsultorbtn.addActionListener(listener);
        consultor.generarCodigosBarrasVentaConsultorbtn.addActionListener(listener);
        consultor.buscarClienteNombreConsultorbtn.addActionListener(listener);
        consultor.buscarProductoPorCodigobtn.addActionListener(listener);
        consultor.buscarTrabajadorDiasConsultorbtn.addActionListener(listener);
        consultor.buscarVentaTipobtn.addActionListener(listener);
        consultor.acercaDeItem.addActionListener(listener);
        consultor.ayudaItem.addActionListener(listener);
        //Vendedor
        vendedor.generarVentaVendedorbtn.addActionListener(listener);
        vendedor.eliminarVentaVendedorbtn.addActionListener(listener);
        vendedor.modificarVentaVendedorbtn.addActionListener(listener);
        vendedor.generarPDFVentaVendedorbtn.addActionListener(listener);
        vendedor.generarExcelVentaVendedorbtn.addActionListener(listener);
        vendedor.regresoVentasbtn.addActionListener(listener);
        vendedor.enviarMailVendedorbtn.addActionListener(listener);
        vendedor.generarCodigosBarrasVendedorbtn.addActionListener(listener);
        vendedor.buscarVentaPorTipobtn.addActionListener(listener);
        vendedor.acercaDeItem.addActionListener(listener);
        vendedor.ayudaItem.addActionListener(listener);



    }


    /**
     * Metodo de que se encarga de rellenar los combos de trabajador y cliente del administrador
     */
    public void rellenarCombos(){
        admin.comboTrabajador.removeAllItems();
        modelo.setComboBoxTrabajador(modelo.getTrabajador(),admin.comboTrabajador);
        admin.comboCliente.removeAllItems();
        modelo.setComboBoxCliente(modelo.getCliente(),admin.comboCliente);

    }
    /**
     * Metodo de que se encarga de rellenar los combos de trabajador y cliente del vendedor
     */
    public void rellenarCombosVendedor(){
        vendedor.comboTrabajadorVendedor.removeAllItems();
        modelo.setComboBoxTrabajador(modelo.getTrabajador(),vendedor.comboTrabajadorVendedor);
        vendedor.comboClienteVendedor.removeAllItems();
        modelo.setComboBoxCliente(modelo.getCliente(),vendedor.comboClienteVendedor);

    }

    /**
     * Metodo que se encarga de darle funcionalidas a las listas de cada clase
     * Para que de esta manera puedan mostrar todos sus datos
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        admin.listCliente.addListSelectionListener(listener);
        admin.listProducto.addListSelectionListener(listener);
        admin.listTrabajador.addListSelectionListener(listener);
        admin.listVenta.addListSelectionListener(listener);
        admin.listNuevoUsuarioAdmin.addListSelectionListener(listener);
        admin.listClienteProducto.addListSelectionListener(listener);
        admin.listProductoCliente.addListSelectionListener(listener);
        admin.listTrabajadorProducto.addListSelectionListener(listener);
        admin.listProductoTrabajador.addListSelectionListener(listener);
        //Listas Consultor
        consultor.listClientesConsultor.addListSelectionListener(listener);
        consultor.listProductosConsultor.addListSelectionListener(listener);
        consultor.listTrabajadorConsultor.addListSelectionListener(listener);
        consultor.listVentasConsultor.addListSelectionListener(listener);
        //ListaVendedor
        vendedor.listVentaVendedor.addListSelectionListener(listener);
    }

    /**
     * El método valueChanged muestra el primer
     * y último índices reportados por el evento,
     * el valor de la bandera isAdjusting del evento, y el indice actualmente seleccionado.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == admin.listCliente) {
                Cliente cliente = (Cliente) admin.listCliente.getSelectedValue();
                admin.txtNombreCliente.setText(cliente.getNombreCliente());
                admin.txtApellidoCliente.setText(cliente.getApellidosCliente());
                admin.txtTelefonoCliente.setText(cliente.getApellidosCliente());
                admin.txtTelefonoCliente.setText(cliente.getTelefono());
                admin.txtEmailCliente.setText(cliente.getEmail());
                admin.txtDniCliente.setText(cliente.getDni());
                admin.txtDireccionCliente.setText(cliente.getDireccion());
                admin.comboEstadoCliente.setSelectedItem(cliente.getEstado());
                admin.txtNumeroCliente.setText(String.valueOf(cliente.getNumeroCliente()));
            }
            if(e.getSource() == admin.listProducto) {
                Producto producto = (Producto) admin.listProducto.getSelectedValue();
                admin.txtCodigoProducto.setText(String.valueOf(producto.getCodigoProducto()));
                admin.txtNombreProducto.setText(producto.getNombre());
                admin.txtLaboratorio.setText(producto.getLaboratorio());
                admin.comboEstadoProducto.setSelectedItem(producto.getEstado());
                admin.comboTipoProducto.setSelectedItem(producto.getTipo());
                admin.txtStock.setText(producto.getStock());
                admin.txtPrecioProducto.setText(String.valueOf(producto.getPrecio()));
                admin.datePickerProducto.setDate(producto.getFechaFabricacion().toLocalDate());
                listado.listarClientesProducto(modelo.getCliente(),admin);
                listado.listarTrabajadorProducto(modelo.getTrabajador(),admin);
            }
            if(e.getSource() == admin.listTrabajador) {
                Trabajador trabajador = (Trabajador) admin.listTrabajador.getSelectedValue();
                admin.txtNombreTrabajador.setText(trabajador.getNombreTrabajador());
                admin.txtApellidosTrabajador.setText(trabajador.getApellidosTrabajador());
                admin.txtDniTrabajador.setText(trabajador.getDniTrabajador());
                admin.txtSueldoTrabajador.setText(String.valueOf(trabajador.getSueldoTrabajador()));
                admin.txtDiasTrabajados.setText(String.valueOf(trabajador.getDiasTrabajados()));
                admin.txtTelefonoTrabajador.setText(trabajador.getTelefonoTrabajador());
                admin.txtDireccionTrabajador.setText(trabajador.getDireccionTrabajador());
                admin.comboCargoTrabajador.setSelectedItem(trabajador.getCargoTrabajador());

            }
            if(e.getSource() == admin.listVenta) {
                Venta venta = (Venta) admin.listVenta.getSelectedValue();
                admin.comboEstadoVenta.setSelectedItem(venta.getEstadoVenta());
                admin.txtNumeroClienteVenta.setText(String.valueOf(venta.getNumeroClienteVenta()));
                admin.txtCodigoProductoVenta.setText(String.valueOf(venta.getCodigoProductoVenta()));
                admin.txtIvaVenta.setText(String.valueOf(venta.getIva()));
                admin.comboTipoPagoVenta.setSelectedItem(venta.getTipoPago());
                admin.dateFechaVenta.setDate(venta.getFechaVenta().toLocalDate());
                admin.comboTipoMonedaVenta.setSelectedItem(venta.getTipoMoneda());
                admin.comboCliente.setSelectedItem(venta.getCliente());
                admin.comboTrabajador.setSelectedItem(venta.getTrabajador());


            }

            if(e.getSource() == admin.listNuevoUsuarioAdmin) {
                Registro registro = (Registro) admin.listNuevoUsuarioAdmin.getSelectedValue();
                admin.txtNuevoUsuarioAdmin.setText(registro.getNombreUsuario());
                admin.txtNuevaPasswordAdmin.setText(registro.getPasswordRegistro());
                admin.txtNuevaPasswordAdmin.setEnabled(false);
                admin.comboTiposUsuario.setSelectedItem(registro.getTipoUsuario());



            }
            if(e.getSource() == vendedor.listVentaVendedor) {
                Venta venta = (Venta) vendedor.listVentaVendedor.getSelectedValue();
                vendedor.comboEstadoVentaVendedor.setSelectedItem(venta.getEstadoVenta());
                vendedor.txtNumeroClienteVentaVendedor.setText(String.valueOf(venta.getNumeroClienteVenta()));
                vendedor.txtCodigoProductoVentaVendedor.setText(String.valueOf(venta.getCodigoProductoVenta()));
                vendedor.txtIvaVentaVendedor.setText(String.valueOf(venta.getIva()));
                vendedor.comboTipoPagoVentaVendedor.setSelectedItem(venta.getTipoPago());
                vendedor.dateFechaVentaVendedor.setDate(venta.getFechaVenta().toLocalDate());
                vendedor.comboTipoMonedaVentaVendedor.setSelectedItem(venta.getTipoMoneda());
                vendedor.comboClienteVendedor.setSelectedItem(venta.getCliente());
                vendedor.comboTrabajadorVendedor.setSelectedItem(venta.getTrabajador());


            }
        }

    }

    /**
     * Metodo que comprueba si una cadena de texto es de valor numerico
     * @param cadena
     * @return
     */
    public boolean comprobanteValorNumerico(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }

    /**
     * Metodo que comprueba la estructura del dni
     * añadiendo que el dni tiene que ser de 8 digitos y una letra por defecto
     * @param dni
     * @return
     */
    public boolean comprobanteDni(String dni) {
        Pattern pattern=null;
        Matcher matcher=null;
        pattern = Pattern.compile("[0-9]{8,9}[A-Z]");
        matcher=pattern.matcher(dni);
        if(matcher.find()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Metodo que comprueba la estructura de los email añadiendoles que debe incluir una @ y un .
     * @param email
     * @return
     */
   public boolean comprobanteEmail(String email){
       Pattern pattern=null;
       Matcher matcher=null;
       pattern=Pattern.compile("^[\\w\\-\\_\\+]+(\\.[\\w\\-\\_]+)*@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$");
       matcher=pattern.matcher(email);
       if(matcher.find()){
           return true;
       }else{
           return false;
       }
   }



}

