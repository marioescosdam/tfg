package com.marioea.farmaciapaladium.gui;



import com.marioea.farmaciapaladium.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import javax.swing.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Modelo {
    //crear el objeto SessionFactory que mediante el método SessionFactory.openSession() nos dará acceso a Session
    SessionFactory sessionFactory;
    //Variable de conexion para conectar con la bbdd
    static Connection conexion;


    /**
     * Metodo que sirve para cerrar la conexion con nuestra base de datos
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if (sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo que sirve para establecer una conexion con nuestra base de datos
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Registro.class);
        configuracion.addAnnotatedClass(Trabajador.class);
        configuracion.addAnnotatedClass(Venta.class);



        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

        try {
            //Mediante la variable de conexion llamamos al drive manager y cogemos la url de la base de datos, usuario y contraseña
            conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que se encarga de comprobar
     * el usuario,contraseña y el tipo de usuario en la base de datos
     * Este metodo lo he utilizado para comprobar que el tipo de usuario contraseña y usuario es el adecuado a la hora de iniciar sesion
     * y puede pasar a una ventana o otra dependiendo del tipo de usuario seleccionado
     * @param user
     * @param password
     * @param tipoUsuario
     * @return
     * @throws SQLException
     */
    public static boolean comprobante(String user, String password, String tipoUsuario) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            String consulta = "SELECT * FROM registro where nombre_usuario= ? and password_registro= ? and tipo_usuario= ?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, user);
            sentencia.setString(2, password);
            sentencia.setString(3,tipoUsuario);

            ResultSet resultado = sentencia.executeQuery();

            if (resultado.next()) {
                return true;
            }

            return false;
        } catch (SQLException a) {
            a.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException a) {
                    a.printStackTrace();
                }
            }
        }
        return false;
    }


    /**
     * Metodo que se encarga de comprobar la existencia
     * de un usuario en la base de datos, mediante una consulta que selecciona todos los datos de registro
     * @return
     */


    public static boolean comprobarExistenciaUsuario() {
        boolean res = false;
        PreparedStatement sentencia = null;
        String verificar = "SELECT * FROM registro";
        try{
            sentencia = conexion.prepareStatement(verificar);
            ResultSet resultado = sentencia.executeQuery();
            if(resultado.next()){
                res = true;
            }else{
                res=false;
            }

        } catch(Exception e){
            System.err.print("Ha ocurrido un error: "+ e.getMessage());
        }
        finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException a) {
                    a.printStackTrace();
                }
            }
        }
        return res;
    }


    /**
     * Metodo que se encarga de dar de alta a un cliente
     * @param nuevoCliente
     */
    public void altaCliente(Cliente nuevoCliente){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo que se encarga de dar de alta a un producto
     * @param nuevoProducto
     */
    public void altaProducto(Producto nuevoProducto){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo que se encarga de dar de alta a un trabajador
     * @param nuevoTrabajador
     */
    public void altaTrabajador(Trabajador nuevoTrabajador){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoTrabajador);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo que se encarga de dar de alta a una venta
     * @param nuevaVenta
     */
    public void altaVenta(Venta nuevaVenta){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaVenta);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    /**
     * Metodo que se encarga de dar de alta a un usuario
     * @param nuevoRegistro
     */
    public void altaUsuario(Registro nuevoRegistro){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoRegistro);
        sesion.getTransaction().commit();
        sesion.clear();
    }


    /**
     * Metodo que se encarga de borrar a un cliente
     * @param borrarCliente
     */
    public void eliminarCliente(Cliente borrarCliente){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(borrarCliente);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que se encarga de borrar a un producto
     * @param borrarProducto
     */
    public void eliminarProducto(Producto borrarProducto){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(borrarProducto);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que se encarga de borrar a un trabajador
     * @param borrarTrabajador
     */
    public void eliminarTrabajador(Trabajador borrarTrabajador){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(borrarTrabajador);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que se encarga de borrar a una venta
     * @param borrarVenta
     */
    public void eliminarVenta(Venta borrarVenta){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(borrarVenta);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que se encarga de borrar a un usuario
     * @param borrarRegistro
     */
    public void eliminarUsuario(Registro borrarRegistro){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(borrarRegistro);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que se encarga de modificar a un cliente
     * @param modificarCliente
     */
    public void modificarCliente(Cliente modificarCliente) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(modificarCliente);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que se encarga de modificar a un producto
     * @param modificarProducto
     */
    public void modificarProducto(Producto modificarProducto) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(modificarProducto);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que se encarga de modificar a un trabajador
     * @param modificarTrabajador
     */
    public void modificarTrabajador(Trabajador modificarTrabajador) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(modificarTrabajador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que se encarga de modificar a una venta
     * @param modificarVenta
     */
    public void modificarVenta(Venta modificarVenta) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(modificarVenta);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que se encarga de modificar un usuario
     * @param modificarRegistro
     */
    public void modificarUsuario(Registro modificarRegistro) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(modificarRegistro);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que se encarga de coger los datos de cliente para listarlos
     * @return
     */
    public ArrayList<Cliente> getCliente(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Cliente");
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de buscar en la lista de cliente
     * por nombre de cliente
     * @param nombreCliente
     * @return
     */
    public ArrayList<Cliente> getClienteEstado(String nombreCliente){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Cliente c where c.nombreCliente=:nombreCliente");
        query.setParameter("nombreCliente", nombreCliente);
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de coger los datos de producto para listarlos
     * @return
     */
    public ArrayList<Producto> getProducto(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Producto");
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de buscar en la lista de producto
     * por codigo de producto
     * @param codigoProducto
     * @return
     */
    public ArrayList<Producto> getProductoCodigo(int codigoProducto){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Producto p where p.codigoProducto=:codigoProducto");
        query.setParameter("codigoProducto", codigoProducto);
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de coger los datos de trabajador para listarlos
     * @return
     */
    public ArrayList<Trabajador> getTrabajador(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Trabajador");
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de buscar en la lista de trabajador
     * por dias trabajados
     * @param diasTrabajados
     * @return
     */
    public ArrayList<Trabajador> getTrabajadorDias(String diasTrabajados){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Trabajador t where t.diasTrabajados=:diasTrabajados");
        query.setParameter("diasTrabajados", diasTrabajados);
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de coger los datos de venta para listarlos
     * @return
     */
    public ArrayList<Venta> getVenta(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Venta");
        ArrayList<Venta> lista = (ArrayList<Venta>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de buscar en la lista de venta
     * por tipo de moneda
     * @param tipoMoneda
     * @return
     */
    public ArrayList<Venta> getVentaTipoMoneda(String tipoMoneda){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Venta v where v.tipoMoneda=:tipoMoneda");
        query.setParameter("tipoMoneda", tipoMoneda);
        ArrayList<Venta> lista = (ArrayList<Venta>) query.getResultList();
        session.clear();
        return lista;
    }
    /**
     * Metodo que se encarga de coger los datos de registro para listarlos
     * @return
     */
    public ArrayList<Registro> getRegistro(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Registro");
        ArrayList<Registro> lista = (ArrayList<Registro>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que se encarga de guardar en una lista cada uno de los trabajadores
     * y los añade a un combo, para que a la hora de clickar sobre el combo se muestren todos los datos del combo.
     * @param lista
     * @param combo
     */
    public void setComboBoxTrabajador(ArrayList<Trabajador> lista, JComboBox combo){
        for (Trabajador trabajador: lista) {
            combo.addItem(trabajador);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo que se encarga de guardar en una lista cada uno de los clientes
     * y los añade a un combo, para que a la hora de clickar sobre el combo se muestren todos los datos del combo.
     * @param lista
     * @param combo
     */
    public void setComboBoxCliente(ArrayList<Cliente> lista, JComboBox combo){
        for (Cliente cliente: lista) {
            combo.addItem(cliente);
        }
        combo.setSelectedIndex(-1);
    }
}
