package com.marioea.farmaciapaladium.consultor;

import com.marioea.farmaciapaladium.registro.LoginScreen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
/**
 * Esta ventana corresponde al usuario con un permiso de consultor
 * En ella se podran visualizar las diferentes listas de todas las clases y tambien se mandaran indicencias mediante el correo electronico
 */
public class ConsultorScreen extends JFrame  {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JList listClientesConsultor;
    public JButton listarProductosConsultorbtn;
    public JList listProductosConsultor;
    public JButton listarClientesConsultorbtn;
    public JButton listarTrabajadorConsultorbtn;
    public JList listTrabajadorConsultor;
    public JButton listarVentasConsultorbtn;
    public JList listVentasConsultor;
    public JButton listarClienteProductoConsultorbtn;
    public JList listClienteProductoConsultor;
    public JButton listarTrabajadorProductoConsultorbtn;
    public JList listTrabajadorProductoConsultor;
    public JButton regresoConsultorbtn;
    public JTextField txtDestinatarioConsultor;
    public JTextField txtAsuntoConsultor;
    public JButton enviarMailConsultorbtn;
    public JTextArea textAreaMensajeConsultor;
    public JButton generarPDFClienteConsultorbtn;
    public JButton generarExcelClientebtn;
    public JButton generarPdfProductoConsultorbtn;
    public JButton generarExcelProductosConsultorbtn;
    public JButton generarPdfTrabajadorConsultorbtn;
    public JButton generarExcelTrabajadorConsultorbtn;
    public JButton generarPdfVentasConsultorbtn;
    public JButton generarExcelVentasConsultorbtn;
    public JButton diagramaClienteConsultorbtn;
    public JButton diagramaProductoConsultorbtn;
    public JButton diagramaTrabajadorConsultorbtn;
    public JButton diagramaVentaConsultorbtn;
    public JButton generarCodigosBarrasConsultorbtn;
    public JButton generarCodigosBarrasVentaConsultorbtn;
    public JButton buscarClienteNombreConsultorbtn;
    public JTextField txtBuscarClienteConsultor;
    public JButton buscarProductoPorCodigobtn;
    public JTextField txtBuscarProductoConsultor;
    public JButton buscarTrabajadorDiasConsultorbtn;
    public JTextField txtBuscarTrabajadorConsultor;
    public JButton buscarVentaTipobtn;
    public JTextField txtBuscarVentaConsultor;
    public JMenuItem acercaDeItem;
    public JMenuItem ayudaItem;

    public  DefaultListModel dlmCliente;
    public  DefaultListModel dlmProducto;
    public  DefaultListModel dlmTrabajador;
    public  DefaultListModel dlmVenta;
    public  DefaultListModel dlmClienteProducto;
    public  DefaultListModel dlmTrabajadorProducto;


    /**
     * Constructor de la clase consultor
     * Donde se marca datos de la ventana como el tamaño, nombre de la ventana
     * y se da funcionamiento al menu de la parte superior y las distintas enumeraciones de la clase consultor
     */
    public ConsultorScreen(){
        this.setTitle("Consultor");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+400, this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("logo.png"));
        this.setIconImage(ico);


        crearMenu();
        crearModelos();


    }
    /**
     * Barra de menu que se genera en la parte superior de la ventana
     * Incluye datos como el acerca de y la ayuda
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");

        acercaDeItem = new JMenuItem("Acerca de");
        acercaDeItem.setActionCommand("Acerca de Consultor");
        ayudaItem = new JMenuItem("Ayuda");
        ayudaItem.setActionCommand("Ayuda Consultor");


        menu.add(acercaDeItem);
        menu.add(ayudaItem);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
    /**
     * Le damos a las listar creadas un default list model
     */
    private void crearModelos() {
        dlmCliente = new DefaultListModel();
        listClientesConsultor.setModel(dlmCliente);
        dlmProducto = new DefaultListModel();
        listProductosConsultor.setModel(dlmProducto);
        dlmTrabajador = new DefaultListModel();
        listTrabajadorConsultor.setModel(dlmTrabajador);
        dlmVenta = new DefaultListModel();
        listVentasConsultor.setModel(dlmVenta);
        dlmClienteProducto = new DefaultListModel();
        listClienteProductoConsultor.setModel(dlmClienteProducto);
         dlmTrabajadorProducto= new DefaultListModel();
        listTrabajadorProductoConsultor.setModel(dlmTrabajadorProducto);


    }


}
