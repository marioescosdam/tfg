package com.marioea.farmaciapaladium;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Producto {
    private int id;
    private int codigoProducto;
    private String nombre;
    private String laboratorio;
    private String estado;
    private String tipo;
    private String stock;
    private double precio;
    private Date fechaFabricacion;
    private List<Trabajador> trabajadores;
    private List<Cliente> clientes;

    public Producto() {
    }

    public Producto(int id, int codigoProducto, String nombre, String laboratorio, String estado, String tipo, String stock, double precio, Date fechaFabricacion) {
        this.id = id;
        this.codigoProducto = codigoProducto;
        this.nombre = nombre;
        this.laboratorio = laboratorio;
        this.estado = estado;
        this.tipo = tipo;
        this.stock = stock;
        this.precio = precio;
        this.fechaFabricacion = fechaFabricacion;
    }
    public Producto(int codigoProducto, String nombre, String laboratorio, String estado, String tipo, String stock, double precio, Date fechaFabricacion) {
        this.codigoProducto = codigoProducto;
        this.nombre = nombre;
        this.laboratorio = laboratorio;
        this.estado = estado;
        this.tipo = tipo;
        this.stock = stock;
        this.precio = precio;
        this.fechaFabricacion = fechaFabricacion;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo_producto")
    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "laboratorio")
    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    @Basic
    @Column(name = "estado")
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "stock")
    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_fabricacion")
    public Date getFechaFabricacion() {
        return fechaFabricacion;
    }

    public void setFechaFabricacion(Date fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                codigoProducto == producto.codigoProducto &&
                Double.compare(producto.precio, precio) == 0 &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(laboratorio, producto.laboratorio) &&
                Objects.equals(estado, producto.estado) &&
                Objects.equals(tipo, producto.tipo) &&
                Objects.equals(stock, producto.stock) &&
                Objects.equals(fechaFabricacion, producto.fechaFabricacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigoProducto, nombre, laboratorio, estado, tipo, stock, precio, fechaFabricacion);
    }

    @ManyToMany(mappedBy = "productos",fetch=FetchType.EAGER)
    @Fetch(value= FetchMode.SUBSELECT)
    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    @ManyToMany(mappedBy = "productos",fetch=FetchType.EAGER)
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
    @Override
    public String toString() {
        return
                "Codigo de Producto: " + codigoProducto +'\''+
                ", Nombre: '" + nombre + '\'' +
                ", Precio: " + precio + '\''+
                        ", Stock: " +stock+'\''+
                ", Fecha de Fabricación: " + fechaFabricacion;
    }
}
