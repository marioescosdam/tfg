package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de Cargos de trabajador de la vista.
 * Representan los diferentes cargos de trabajador de la aplicacion.
 */
public enum EnumeracionCargoTrabajador{
    FARMACEUTICOTITULAR("Farmaceutico Titular"),
    FARMACEUTICOCOTITULAR("Farmaceutico Co-Titular"),
    AUXILIARMAYORDIPLOMADO("Auxiliar Mayor Diplomado"),
    AUXILIARDIPLOMADO("Auxiliar Diplomado"),
    PERSONALAUXILIAR("Personal Auxiliar");



    private String cargoTrabajador;

    EnumeracionCargoTrabajador(String cargoTrabajador) {
        this.cargoTrabajador = cargoTrabajador;
    }

    public String getCargoTrabajador() {
        return cargoTrabajador;
    }
}
