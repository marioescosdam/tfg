package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de TIpo de pago de la vista.
 * Representan los diferentes tipos de pago en la pantalla de venta de la aplicacion.
 */
public enum EnumeracionTipoPago {
    EFECTIVO("Efectivo"),
    TARJETACREDITO("Tarjeta de Credito"),
    PAYPAL("Paypal"),
    TRANSFERENCIABANCARIA("Transferencia Bancaria"),
    PAGOMOVIL("Pago por el Movil");
    private String tipoPago;

    EnumeracionTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getTipoPago() {
        return tipoPago;
    }
}