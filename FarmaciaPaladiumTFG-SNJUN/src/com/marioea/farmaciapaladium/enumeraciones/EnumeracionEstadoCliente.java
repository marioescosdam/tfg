package com.marioea.farmaciapaladium.enumeraciones;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComboBox de Enumeracion de Estados de Cliente de la vista.
 * Representan los diferentes estados en los que se encuentra el cliente de la aplicacion.
 */
public enum EnumeracionEstadoCliente{
    ACTIVO("Activo"),
    BAJA("Baja");

    private String estadoCliente;

    EnumeracionEstadoCliente(String estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    public String getEstadoCliente() {
        return estadoCliente;
    }
}
