package com.marioea.farmaciapaladium.resets;

import com.marioea.farmaciapaladium.Registro;
import com.marioea.farmaciapaladium.admin.AdminScreen;
import com.marioea.farmaciapaladium.registro.LoginRegister;
import com.marioea.farmaciapaladium.registro.LoginScreen;
import com.marioea.farmaciapaladium.vendedor.VendedorScreen;

public class Resets {
    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase cliente
     * @param admin
     */
    public void resetCliente(AdminScreen admin){
        admin.txtNombreCliente.setText("");
        admin.txtApellidoCliente.setText("");
        admin.txtTelefonoCliente.setText("");
        admin.txtEmailCliente.setText("");
        admin.txtDniCliente.setText("");
        admin.txtDireccionCliente.setText("");
        admin.comboEstadoCliente.setSelectedItem("");
        admin.txtNumeroCliente.setText("");
    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase producto
     * @param admin
     */
    public void resetProducto(AdminScreen admin){
        admin.txtCodigoProducto.setText("");
        admin.txtNombreProducto.setText("");
        admin.txtLaboratorio.setText("");
        admin.comboEstadoProducto.setSelectedItem("");
        admin.comboTipoProducto.setSelectedItem("");
        admin.txtStock.setText("");
        admin.txtPrecioProducto.setText("");
        admin.datePickerProducto.setText("");
    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase trabajador
     * @param admin
     */
    public void resetTrabajador(AdminScreen admin){
        admin.txtNombreTrabajador.setText("");
        admin.txtApellidosTrabajador.setText("");
        admin.txtDniTrabajador.setText("");
        admin.txtSueldoTrabajador.setText("");
        admin.txtDiasTrabajados.setText("");
        admin.txtTelefonoTrabajador.setText("");
        admin.txtDireccionTrabajador.setText("");
        admin.comboCargoTrabajador.setSelectedItem("");
    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase venta
     * @param admin
     */
    public void resetVenta(AdminScreen admin){
        admin.comboEstadoCliente.setSelectedItem("");
        admin.txtApellidoCliente.setText("");
        admin.txtCodigoProductoVenta.setText("");
        admin.txtIvaVenta.setText("");
        admin.comboTipoPagoVenta.setSelectedItem("");
        admin.dateFechaVenta.setText("");
        admin.comboTipoMonedaVenta.setSelectedItem("");
        admin.comboTrabajador.setSelectedItem("");
        admin.comboCliente.setSelectedItem("");
    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase registro
     * @param register
     */
    public void resetRegistro(LoginRegister register){
        register.txtNuevoUsuario.setText("");
        register.txtNuevaPassword.setText("");
        register.comboTiposUsuario.setSelectedItem("");

    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase registro
     * de la ventana de administrador
     * @param admin
     */
    public void resetRegistroAdmin(AdminScreen admin){
        admin.txtNuevoUsuarioAdmin.setText("");
        admin.txtNuevaPasswordAdmin.setText("");
        admin.comboTiposUsuario.setSelectedItem("");

    }

    /**
     * Metodo que se encarga de resetear
     * o mas bien limpiar todos textfields de la clase venta de la ventana de vendedor
     * @param vendedor
     */
    public void resetVentaVendedor(VendedorScreen vendedor){
        vendedor.comboEstadoVentaVendedor.setSelectedItem("");
        vendedor.txtNumeroClienteVentaVendedor.setText("");
        vendedor.txtCodigoProductoVentaVendedor.setText("");
        vendedor.txtIvaVentaVendedor.setText("");
        vendedor.comboTipoPagoVentaVendedor.setSelectedItem("");
        vendedor.dateFechaVentaVendedor.setText("");
        vendedor.comboTipoMonedaVentaVendedor.setSelectedItem("");
        vendedor.comboTrabajadorVendedor.setSelectedItem("");
        vendedor.comboClienteVendedor.setSelectedItem("");
    }
}
