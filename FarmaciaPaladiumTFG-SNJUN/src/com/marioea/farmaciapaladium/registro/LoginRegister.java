package com.marioea.farmaciapaladium.registro;

import javax.swing.*;
import java.awt.*;
/**
 * Esta ventana salta cuando no existe ningun usuario en la base de datos y nos obliga a crear un administrador
 * ya que sin un administrador la aplicacion no puede funcionar
 */
public class LoginRegister extends JFrame{
    private JPanel panel1;
    public JTextField txtNuevoUsuario;
    public JPasswordField txtNuevaPassword;
    public JComboBox comboTiposUsuario;
    public JButton nuevoUsuariobtn;




    /**
     * Constructor de la clase login register
     * Donde se marca datos de la ventana como el tamaño, nombre de la ventana
     * y las distintas enumeraciones de la clase de registro
     */
    public LoginRegister(){
        this.setTitle("Farmacia Paladium");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+400, this.getHeight()));
        this.setLocationRelativeTo(null);
        Image ico=Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("logo.png"));
        this.setIconImage(ico);

        enumeracion();



    }

    /**
     * Metodo que coge los datos de la clase enumerada de tipos de usuario
     * y los almacena en una combobox
     */
    public void enumeracion(){
        for (EnumeracionTipos enumeracionTipos: EnumeracionTipos.values()){
            comboTiposUsuario.addItem(enumeracionTipos.getPermiso());
        }
        comboTiposUsuario.setSelectedItem(-1);
    }
}
