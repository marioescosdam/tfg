package com.marioea.farmaciapaladium.codigosbarras;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.PdfWriter;
import com.marioea.farmaciapaladium.Producto;
import com.marioea.farmaciapaladium.Venta;
import com.marioea.farmaciapaladium.admin.AdminScreen;
import com.marioea.farmaciapaladium.gui.Modelo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.*;

/**
 * Clase que se encarga de generar pdfs de los productos en formato de pdf
 */
public class CodigoBarras {
    //declaramos una variable de conexion
Connection connection;

    public void GenerarCodigoBaras(Modelo modelo) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        Image img;
        //mediante la variable de conexion realizamos la conexion con la bbddd y añadimos el usuario y contraseña para acceder a la bbdd
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //mediante el prepareStatement creamos una consulta de base de datos en la
            // cual cogemos todos los datos de la tabla producto
            //en la bbdd
            ps=connection.prepareStatement("SELECT * FROM producto");
            //ejecutamos la consulta
            rs=ps.executeQuery();
            //Creamos un documento
            Document doc = new Document();
            //creamos un pdf writer y coge de la instancia el documento y crea un fichero
            // salvo que exista o sea de lectura con el formato de pdf
            PdfWriter pdf = PdfWriter.getInstance(doc,new FileOutputStream("codigosProductos.pdf"));
            //abre el documento creado
            doc.open();

            //Creamos un barcode para establecer el tipo de formato de codigo de barras que queremos
            Barcode39 code= new Barcode39();
            while(rs.next()){

            //cogemos de la base de datos para formar el codigo de barras el codigo del producto
            code.setCode(rs.getString("codigo_producto"));
            //añadimos en el pdf una imagen de codigo de barras generada
            img =code.createImageWithBarcode(pdf.getDirectContent(), BaseColor.BLACK,BaseColor.BLACK);
            //añadimos la imagen al documento
            doc.add(img);

            doc.add(new Paragraph(" "));
            }
            doc.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void GenerarCodigoBarasVendedor(Modelo modelo) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        Image img;
        //mediante la variable de conexion realizamos la conexion con la bbddd y añadimos el usuario y contraseña para acceder a la bbdd
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/farmaciapaladium", "root", "");
        try {
            //mediante el prepareStatement creamos una consulta de base de datos en la
            // cual cogemos todos los datos de la tabla venta
            //en la bbdd
            ps=connection.prepareStatement("SELECT * FROM venta");
            //ejecutamos la consulta
            rs=ps.executeQuery();
            //Creamos un documento
            Document doc = new Document();
            //creamos un pdf writer y coge de la instancia el documento y crea un fichero
            // salvo que exista o sea de lectura con el formato de pdf
            PdfWriter pdf = PdfWriter.getInstance(doc,new FileOutputStream("codigosProductosVenta.pdf"));
            //abrimos el documento
            doc.open();

            //Creamos un barcode para establecer el tipo de formato de codigo de barras que queremos
            Barcode39 code= new Barcode39();
            while(rs.next()){

                //cogemos de la base de datos para formar el codigo de barras el codigo del producto de la venta
                code.setCode(rs.getString("codigo_producto_venta"));
                //añadimos en el pdf una imagen de codigo de barras generada
                img =code.createImageWithBarcode(pdf.getDirectContent(), BaseColor.BLACK,BaseColor.BLACK);
                //añadimos la imagen al documento
                doc.add(img);

                doc.add(new Paragraph(" "));
            }
            doc.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
