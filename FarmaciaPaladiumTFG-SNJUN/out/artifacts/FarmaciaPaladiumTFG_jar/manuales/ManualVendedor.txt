********************MANUAL VENDEDOR**************************
**********VENTA*********
Dentro de la venta encontramos diferentes opciones que el vendedor puede realizar como dar de alta, para ello el vendedor tiene que rellenar cada textfield y combobox de la ventana de venta.
Aquí debemos tener en cuenta el combo de trabajador y cliente, ya que para que estos funcionen debemos tener un cliente y un trabajador creado anteriormente.
Para realizar una modificación, el vendedor debe presionar en la lista de ventas la venta que desea modificar y cambiar el dato en el textfield correspondiente y para eliminar debe marcar el campo a eliminar en la lista y darle al botón de eliminar.
El vendedor también podrá generar pdf y excel de todos los datos de la lista o también un diagrama sobre un dato de la lista.
En esta ventana de productos el vendedor puede generar un pdf con todos los códigos de barras de todas las ventas.
Por último, el vendedor puede realizar una búsqueda por tipo de moneda añadiendo el tipo de moneda en el textfield y dándole al botón.


**********INCIDENCIAS*********
En esta pestaña, el vendedor tiene la opción de reportar cualquier incidencia al soporte para que sea corregida cuanto antes, para ello debemos rellenar el asunto y cuerpo del mensaje y darle al botón de enviar, una vez realizado todo esto el soporte recibirá un correo en su bandeja de entrada.