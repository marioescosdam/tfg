********************MANUAL CONSULTOR**************************
*********TABLAS*********
Las pestañas de Cliente y Producto en las cuales el consultor puede listar cada una de las tablas que hemos rellenado anteriormente en el administrador y los datos se encuentran almacenados en la base de datos.
El consultor podrá realizar búsquedas tanto en el caso de clientes por nombre de cliente como en el caso de productos por su código.
También podemos listar en el caso de Cliente los clientes que tienen asignados un producto.
Por último, en ambas tablas podemos generar pdfs y Excel de cada una de las tablas.
Trabajador y Venta en las cuales el consultor puede listar cada una de las tablas que hemos rellenado anteriormente en el administrador y los datos se encuentran almacenados en la base de datos.
El consultor podrá realizar búsquedas tanto en el caso de trabajador por días trabajados del trabajador como en el caso de venta por el tipo de moneda.
También podemos listar en el caso de Trabajador los trabajadores que tienen asignados un producto.
Por último, en ambas tablas podemos generar pdfs y Excel de cada una de las tablas.

*********INCIDENCIAS*********
En esta pestaña, el consultor tiene la opción de reportar cualquier incidencia al soporte para que sea corregida cuanto antes, para ello debemos rellenar el asunto y cuerpo del mensaje y darle al botón de enviar, una vez realizado todo esto el soporte recibirá un correo en su bandeja de entrada.



*********OTRAS EXPORTACIONES*********
En la pestaña de otras Exportaciones, el consultor puede realizar diagramas de queso de uno de los campos rellenados y también generar códigos de barras tanto de producto como de venta.